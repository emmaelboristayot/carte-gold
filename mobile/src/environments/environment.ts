// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: true,
  api: 'https://api-cartegold.lanormandiepourchrist.com/index.php/user',
  enseignement: {
    audio: 'https://lanormandiepourchrist.com/enseignements/audio/',
    image: 'https://lanormandiepourchrist.com/enseignements/images/themes/',
  },
  bible: {
    audio: 'https://lanormandiepourchrist.com/enseignements/audio/bible',
    image: 'https://lanormandiepourchrist.com/enseignements/images/themes/bible/'
  },
  adoration: {
    audio: 'https://lanormandiepourchrist.com/enseignements/audio/adorations/',
    image: 'https://lanormandiepourchrist.com/enseignements/images/themes/adorations/cover.png'
  },
  default_password: '393f5f46b947ee804209014a9beb11dc'
};

export function getUrlAudio(trackType: string, track: any){
  
  switch (trackType) {
      case 'BIBLE':
          return `${environment.bible.audio}/${encodeURI(track.url)}`;
      case 'ADORATION':
         return `${environment.adoration.audio}/${encodeURI(track.url)}`;
      case 'ENSEIGNEMENT':
          let year = new Date(track.date).getFullYear();
          return `${environment.enseignement.audio + year}/${encodeURI(track.url)}`;                
  }
}

export function getUrlImage(trackType: string, track: any){
  
  switch (trackType) {
      case 'BIBLE':
          return 'assets/bible-cover.png';
      case 'ADORATION':
         return 'assets/audio-display-background.png';
      case 'ENSEIGNEMENT':
          let cover = (track.th_image !== '') ? environment.enseignement.image + track.th_image : 'assets/audio-display-background.png';
          return cover;                
  }
}
