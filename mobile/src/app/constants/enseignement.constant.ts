import { Enseignement } from '../models/enseignement.model';

export class EnseignementConstant {
  
  public static DEFAULT: Enseignement = {
     id: 0,
     titre:'',
     soustitre:'',
     url:'',
     resume:'',
     image:'',
     date:'',
     vues: 0,
     aut_id: 0,
     aut_nom:'',
     aut_prenom:'',
     aut_photo:'',
     aut_fonction:'',
     aut_description:'',
     th_id:0,
     th_titre:'',
     th_description:'',
     th_image:''
  }
    
}
