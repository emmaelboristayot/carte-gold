
import { Component } from '@angular/core';
import { TeachingPageComponent } from '../teaching-page/teaching-page.component';
import { AdorationsComponent } from '../adorations/adorations.component';
import { BibleAudioComponent } from '../bible-audio/bible-audio.component';

@Component({
  selector: 'app-tabs-page',
  templateUrl: './tabs-page.component.html'
})
export class TabsPage {
  enseignementsPage: any;
  adorationsPage: any;
  bibleAudioPage: any;

  constructor() {
    this.enseignementsPage  = TeachingPageComponent;
    this.adorationsPage     = AdorationsComponent;
    this.bibleAudioPage     = BibleAudioComponent;
  }
}