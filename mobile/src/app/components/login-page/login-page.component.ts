import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../../services/user.service';
import { Store } from '@ngrx/store'

//reducer
import * as authReducer from './../../reducers/auth/auth.reducer';
import * as authAction from './../../reducers/auth/auth.actions';

//pages
import { TabsPage } from '../tabs-page/tabs-page.component';
//native
import { NativeStorage } from '@ionic-native/native-storage';
import { Authenticate } from '../../models/user.model';

@Component({
  selector: 'login-page',
  templateUrl: './login-page.component.html'
})
export class LoginPageComponent implements OnInit {
  user: FormGroup;

  constructor(private fb: FormBuilder,
    private _userService: UserService,
    private _store: Store<authReducer.State>,
    private nativeStorage: NativeStorage,
  ) {
    this._store.select(authReducer.getUser);

    this.user = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]]

    });

    
  }

  ngOnInit() {
    this.nativeStorage.getItem('authenticate')
    .then((data: Authenticate) => {
      this.user.setValue(data);
    },
    error => {
      console.log('error authenticate', error);
    });

  }

  onSubmit() {
    this.nativeStorage.setItem('authenticate', this.user.value)
      .then(() => console.log('Stored item!'),
      error => console.error('Error storing item', error));

    this._store.dispatch(new authAction.Login(this.user.value));
  }
}
