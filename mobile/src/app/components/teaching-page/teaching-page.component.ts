import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store'

//model
import { Enseignement } from '../../models/enseignement.model';
import { Theme } from '../../models/theme.model';
import { TrackPlayed } from '../../models/track-played.model';
//constant
import { environment, getUrlImage } from '../../../environments/environment';

import { FilterByPipe } from 'ngx-pipes/src/app/pipes/array/filter-by';

//Reducers
import * as enseignementsReducer from './../../reducers/enseignements/enseignements.reducer';
import * as searchReducer from './../../reducers/search/search.reducer';
import * as themesReducer from './../../reducers/themes/themes.reducer';
import * as trackPlayedReducer from './../../reducers/track-played/track-played.reducer';

//Actions
import * as enseignementsActions from './../../reducers/enseignements/enseignements.actions';
import * as themesActions from './../../reducers/themes/themes.actions';
//Ionic
import { NavController, ModalController, Platform } from 'ionic-angular';

import { PlayerModalComponent } from '../audio-player/player-modal/player-modal.component';
import { ThemeTeachingComponent } from '../theme-teaching/theme-teaching.component';

import { Observable } from 'rxjs/Observable';
import { retryWhen } from 'rxjs/operators/retryWhen';

@Component({
  selector: 'app-teaching-page',
  templateUrl: './teaching-page.component.html',
  providers: [FilterByPipe]
})
export class TeachingPageComponent implements OnInit {

  public themes$: Observable<Array<Theme>>;
  public enseignements$: Observable<Array<Enseignement>>;

  public searchValue: string;

  public segment: string = "themes";

  public trackPlayed: TrackPlayed;
  public readonly trackType: string = "ENSEIGNEMENT";

  public env: any;

  constructor(
    public navCtrl: NavController,
    public modalCtrl: ModalController,
    private _store_search: Store<searchReducer.State>,
    private _store_enseignements: Store<enseignementsReducer.State>,
    private _store_track_played: Store<trackPlayedReducer.State>,
    private _store_themes: Store<themesReducer.State>,
    private filterByPipe: FilterByPipe,
    public plt: Platform) {
    this.env = environment;

    this._store_search.select(searchReducer.getSearch).subscribe((data: string) => {
      this.searchValue = data;
    });

    this.themes$ = this._store_themes.pipe(select(themesReducer.selectThemes$));
    this.enseignements$ = this._store_enseignements.pipe(select(enseignementsReducer.selectEnseignements$));

    this._store_track_played.select(trackPlayedReducer.getTrackPlayed).subscribe((data: TrackPlayed) => {
      this.trackPlayed = data;
    })

  }

  ngOnInit() {
    this.getEnseignements();
    this.getThemes();
  }


  getEnseignements() {
    this._store_enseignements.dispatch(new enseignementsActions.LoadEnseignements());
  }

  getEnseignementsByIdTheme(themeId: number, enseignementsList: Array<Enseignement>): Array<Enseignement> {
    let enseignements = this.filterByPipe.transform(
      enseignementsList,
      ['th_id'],
      themeId,
      true
    );
    return enseignements;
  }

  getThemes() {
    this._store_themes.dispatch(new themesActions.LoadThemes());
  }

  doRefresh(refresher) {
    this.getEnseignements();
    this.getThemes();

    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 3000);
  }

  openPlayer() {
    this.openAudioDisplay(this.trackPlayed);
  }

  public openAudioDisplay(trackPlayed: TrackPlayed) {
    let modal = this.modalCtrl.create(PlayerModalComponent, trackPlayed);
    modal.present();
  }

  public goToTeaching(themeId: number) {
    this.enseignements$.subscribe((data) => {
      let enseignements = this.filterByPipe.transform(
        data,
        ['th_id'],
        themeId,
        true
      );
      if ( enseignements.length > 0 ) {
        this.navCtrl.push(ThemeTeachingComponent, enseignements);
      }
    });
  }

  getSlidesPerView(): number {
    if (this.plt.is('tablet') || this.plt.isLandscape()) {
      return 4;
    }
    return 2
  }

}
