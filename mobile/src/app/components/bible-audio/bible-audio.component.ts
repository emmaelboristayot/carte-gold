import { BibleAudio } from './../../models/bible-audio.model';
import { NavController, LoadingController, ModalController, Platform, ViewController } from 'ionic-angular';
import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store'

//model
import { Enseignement } from '../../models/enseignement.model';
import { Theme } from '../../models/theme.model';
import { environment } from '../../../environments/environment';
//pipes
import { FilterByPipe } from 'ngx-pipes/src/app/pipes/array/filter-by';

//reducer
import * as trackPlayedReducer from './../../reducers/track-played/track-played.reducer';
import * as trackPlayedAction from './../../reducers/track-played/track-played.actions';

//reducer
import * as bibleAudioReducer from './../../reducers/bible-audio/bible-audio.reducer';
import * as bibleAudioActions from './../../reducers/bible-audio/bible-audio.actions';

//constant
import { bibleThemes, bibleType } from './bible-audio.constant';

import { TrackPlayed } from '../../models/track-played.model';

import { PlayerModalComponent } from '../audio-player/player-modal/player-modal.component';

@Component({
  selector: 'bible-audio',
  templateUrl: './bible-audio.component.html',
  providers: [FilterByPipe]
})
export class BibleAudioComponent implements OnInit {
  public _bibleaudios: Array<BibleAudio>;
  public searchValue: string;
  public trackPlayed: TrackPlayed;
  public env: any;
  public _bibleThemes: Array<any>;
  public _bibleType: Array<any>;
  public readonly trackType: string = "BIBLE";

  constructor(
    private _store_track_played: Store<trackPlayedReducer.State>,
    private _store_bible_audio: Store<bibleAudioReducer.State>,
    public modalCtrl: ModalController,
    public navCtrl: NavController) {
    this.env = environment;
    this._bibleThemes = bibleThemes;
    this._bibleType = bibleType;

    this._store_track_played.select(trackPlayedReducer.getTrackPlayed).subscribe((data: TrackPlayed) => {
      this.trackPlayed = data;
    })

    this._store_bible_audio.select(bibleAudioReducer.selectBibleAudio$).subscribe((data: BibleAudio[]) => {
      this._bibleaudios = data;
    })

  }

  ngOnInit() {
    this.getBibleAudio();
  }
  
  getBibleAudio() {
    this._store_bible_audio.dispatch(new bibleAudioActions.LoadBibleAudio());
  }

  doRefresh(refresher) {
    this.getBibleAudio()

    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 3000);
  }

  openPlayer() {
    this.openAudioDisplay(this.trackPlayed);
  }

  public openAudioDisplay(trackPlayed: TrackPlayed) {
    let modal = this.modalCtrl.create(PlayerModalComponent, trackPlayed);
    modal.present();
  }

}
