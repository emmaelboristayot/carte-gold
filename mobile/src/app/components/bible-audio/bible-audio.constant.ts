export const bibleThemes = [
    {key: 'EVANGILE', value: 'Les Evangiles', TYPE: 'NOUVEAU'},
    {key: 'ACTES', value: 'Actes des Apôtres', TYPE: 'NOUVEAU'},
    {key: 'EPITRE_PAUL', value: 'Epîtres de Paul', TYPE: 'NOUVEAU'},
    {key: 'EPITRE_AUTRES', value: 'Autres Epîtres', TYPE: 'NOUVEAU'},
    {key: 'REVELATION', value: 'Livre de la Révélation', TYPE: 'NOUVEAU'},
    {key: 'PENTATEUQUE', value: 'Le Pentateuque', TYPE: 'ANCIEN'},
    {key: 'HISTORIQUES', value: 'Livres historiques', TYPE: 'ANCIEN'},
    {key: 'POETIQUES', value: 'Livres poétiques', TYPE: 'ANCIEN'},
    {key: 'PROPHETES', value: 'Les Prophètes', TYPE: 'ANCIEN'}
];


export const bibleType = [ 
    {key: 'ANCIEN', value: 'Ancien testament'},
    {key: 'NOUVEAU', value: 'Nouveau testament'}
];
