import { Enseignement } from './../../../models/enseignement.model';
import { Component, Input } from '@angular/core';
import { State, Store } from '@ngrx/store'
import { NavController, NavParams, LoadingController, ModalController, Platform, ViewController } from 'ionic-angular';


//reducer
import * as trackPlayedReducer from './../../../reducers/track-played/track-played.reducer';
import * as trackPlayedAction from './../../../reducers/track-played/track-played.actions';

import { TrackPlayed } from '../../../models/track-played.model';
//Cordova
import { Media, MediaObject } from '@ionic-native/media';
import { AudioPlayerService } from '../audio-player.service';

@Component({
    selector: 'play-pause',
    templateUrl: './play-pause.html'
})


export class PlayPauseComponent {
    public trackPlayed: TrackPlayed;
    public onStatusUpdate: number = 3;

    constructor(public plt: Platform, 
        private _store: Store<trackPlayedReducer.State>,
        public audioPlayerService: AudioPlayerService) {
        this._store.select(trackPlayedReducer.getTrackPlayed).subscribe((data: TrackPlayed) => {
            this.trackPlayed = data;
        })
    }

    ngOnInit() {
        this.onStatusUpdate = this.audioPlayerService.onStatusUpdate;

        this.trackPlayed.player.onStatusUpdate.subscribe(status => { 
            this.onStatusUpdate = status
            this.audioPlayerService.onStatusUpdate = status;
         }); // fires when file status changes
    }

    playAndPause() {
        if (this.onStatusUpdate === 2) {
            this.audioPlayerService.pause(this.trackPlayed);
        }
        else if(this.onStatusUpdate === 3 || this.onStatusUpdate === 4){
            this.audioPlayerService.play(this.trackPlayed);
        }
    }

}
