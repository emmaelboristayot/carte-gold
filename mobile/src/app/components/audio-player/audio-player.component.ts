import {Component} from '@angular/core';


@Component({
  selector: 'audio-player',
  templateUrl: './audio-player.html'
})
export class AudioPlayerComponent {

    public player   : any;
    public duration : number;    // Durée totale
    public time     : number;    // Temps écoulé
    public fraction : number;
    public percent  : number;

    constructor() {    
        this.time       = 0;
        this.duration   = 0;
    }

    repeter() {
        if (this.player.loop) {
            this.player.loop = false;
            this.player.load();
        }
        else {
            this.player.loop = true;
            this.player.load();
        }
    }

    update () {
        this.duration = this.player.duration;    // Durée totale
        this.time     = this.player.currentTime; // Temps écoulé
        this.fraction = this.time / this.duration;
        this.percent  = Math.ceil(this.fraction * 100);
    }

    formatTime(time) {
        let hours : any = Math.floor(time / 3600);
        let mins : any  = Math.floor((time % 3600) / 60);
        let secs : any  = Math.floor(time % 60);
        
        if (secs < 10) {
            secs = "0" + secs;
        }
        
        if (hours) {
            if (mins < 10) {
                mins = "0" + mins;
            }
            
            return hours + ":" + mins + ":" + secs; // hh:mm:ss
        } else {
            return mins + ":" + secs; // mm:ss
        }
    }

    ngOnInit(){
        this.player = document.getElementById('player');
    }

}