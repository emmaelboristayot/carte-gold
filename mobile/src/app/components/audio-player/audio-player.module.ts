import { NgModule }       from '@angular/core';
import { CommonModule }   from '@angular/common';
import { AudioPlayerComponent } from '../audio-player/audio-player.component';
import { AudioTrackComponent } from './audio-track/audio-track.component';
import { ProgressBarComponent } from './progress-bar/progress-bar.component';
import { PlayPauseComponent } from './play-pause/play-pause.component';
import { PlayerModalComponent } from './player-modal/player-modal.component';
import { IonicModule } from 'ionic-angular';


@NgModule({
  imports: [
    CommonModule,
    IonicModule
  ],
  exports: [
    AudioPlayerComponent,
    AudioTrackComponent,
    ProgressBarComponent,
    PlayPauseComponent,
    PlayerModalComponent
  ],
  declarations: [
    AudioPlayerComponent,
    AudioTrackComponent,
    ProgressBarComponent,
    PlayPauseComponent,
    PlayerModalComponent
  ],
  providers: [ ]
})
export class AudioPlayerModule {}