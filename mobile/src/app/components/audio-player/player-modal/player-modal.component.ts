import { Platform, NavParams, ViewController } from 'ionic-angular';
import { Component, OnInit, Input, ViewChild } from '@angular/core';
//model
import { Enseignement } from '../../../models/enseignement.model';
import { environment, getUrlImage } from '../../../../environments/environment';
import { TrackPlayed } from '../../../models/track-played.model';
import { EnseignementConstant } from './../../../constants/enseignement.constant';
import { State, Store } from '@ngrx/store'
//reducer
import * as trackPlayedReducer from './../../../reducers/track-played/track-played.reducer';
import * as trackPlayedAction from './../../../reducers/track-played/track-played.actions';

import { CommonService } from '../common.service';
import { AudioPlayerService } from '../audio-player.service';

@Component({
    selector: 'player-modal',
    templateUrl: './player-modal.html',
})
export class PlayerModalComponent implements OnInit {
    public trackPlayed: TrackPlayed;
    public track: Enseignement = EnseignementConstant.DEFAULT;

    public env: any;
    public onStatusUpdate: number;

    public duration: number = 0;    // Durée totale
    public time: number = 0;    // Temps écoulé

    public rangetime: number = 0;

    public cover: string = '';

    constructor(
        public plt: Platform,
        public platform: Platform,
        public params: NavParams,
        public viewCtrl: ViewController,
        private _store_track_played: Store<trackPlayedReducer.State>,
        public commonService: CommonService,
        public audioPlayerService: AudioPlayerService) {
        this.env = environment;

        this._store_track_played.select(trackPlayedReducer.getTrackPlayed).subscribe((data: TrackPlayed) => {
            this.trackPlayed = data;
            this.track = this.trackPlayed.track;
            this.cover = getUrlImage(this.trackPlayed.trackType, this.trackPlayed.track);
        })
    }

    ngOnInit() {
        let init : number = 0;

        let refreshIntervalId = setInterval(() => {
            this.duration = this.trackPlayed.player.getDuration();
            this.audioPlayerService.duration = this.duration;

            //called once
            if (this.duration !== -1 && init === 0) {
                this.audioPlayerService.initControls();
                init++;
            }

            this.trackPlayed.player.getCurrentPosition().then((position) => {
                this.time = position;
                this.audioPlayerService.time = position;

                this.audioPlayerService.musicControls.updateElapsed({
                    elapsed: position,
                    isPlaying: true
                  });
                  
                this.updateSlider();
            });

        }, 1000)

        this.audioPlayerService.refreshIntervalId = refreshIntervalId;
    }

    fastforward() {
        let newPosition = ((this.time + 30) > this.duration) ? 0 : (this.time - 30) * 1000;
        this.trackPlayed.player.seekTo(newPosition);
    }

    rewind() {
        let newPosition = ((this.time - 30) < 0) ? 0 : (this.time - 30) * 1000;
        this.trackPlayed.player.seekTo(newPosition);
    }

    public updateSlider() {
        this.rangetime = Math.ceil((this.time / this.duration) * 100);
    }

    public onSliderChange($event) {
        let changeTime = (this.duration * $event.value) / 100;
        this.trackPlayed.player.seekTo(changeTime * 1000); //milliseconde
    }

    dismiss() {
        this.viewCtrl.dismiss();
    }

}