// Observable Version
import { Injectable } from '@angular/core';
import { MusicControls } from '@ionic-native/music-controls';
import { TrackPlayed } from '../../models/track-played.model';
import { environment, getUrlImage } from '../../../environments/environment';
import { State, Store } from '@ngrx/store'
//reducer
import * as trackPlayedReducer from './../../reducers/track-played/track-played.reducer';
import * as trackPlayedAction from './../../reducers/track-played/track-played.actions';
//cordova
import { BackgroundMode } from '@ionic-native/background-mode';
import { Platform } from 'ionic-angular';

@Injectable()
export class AudioPlayerService {

  public refreshIntervalId: number;
  public onStatusUpdate: number;

  public duration: number;    // Durée totale
  public time: number;    // Temps écoulé

  public env: any;

  public trackPlayed: TrackPlayed;

  constructor(public musicControls: MusicControls,
     private plt: Platform,
     private backgroundMode: BackgroundMode,
     private _store: Store<trackPlayedReducer.State>) {
    this.env = environment;

    this._store.select(trackPlayedReducer.getTrackPlayed).subscribe((data: TrackPlayed) => {
      this.trackPlayed = data;
    });


  }

  initPlayer() {
    clearInterval(this.refreshIntervalId);
  }

  public initControls() {
    let cover = getUrlImage(this.trackPlayed.trackType, this.trackPlayed.track);
    let artist = (this.trackPlayed.track.aut_nom && this.trackPlayed.track.aut_prenom) ? this.trackPlayed.track.aut_prenom + ' ' + this.trackPlayed.track.aut_nom : '';

    this.musicControls.create({
      track: this.trackPlayed.track.titre,        // optional, default : ''
      artist: artist,                       // optional, default : ''
      cover: cover,      // optional, default : nothing
      // cover can be a local path (use fullpath 'file:///storage/emulated/...', or only 'my_image.jpg' if my_image.jpg is in the www folder of your app)
      //           or a remote url ('http://...', 'https://...', 'ftp://...')
      isPlaying: true,                         // optional, default : true
      dismissable: true,                         // optional, default : false

      // hide previous/next/close buttons:
      hasPrev: false,      // show previous button, optional, default: true
      hasNext: false,      // show next button, optional, default: true
      hasClose: false,       // show close button, optional, default: false

      // iOS only, optional
      album: this.trackPlayed.track.th_titre,     // optional, default: ''
      duration: this.duration, // optional, default: 0
      elapsed: this.time, // optional, default: 0
      hasSkipForward: true,  // show skip forward button, optional, default: false
      hasSkipBackward: true, // show skip backward button, optional, default: false
      skipForwardInterval: 15, // display number for skip forward, optional, default: 0
      skipBackwardInterval: 15, // display number for skip backward, optional, default: 0
      hasScrubbing: true, // enable scrubbing from control center and lockscreen progress bar, optional

      // Android only, optional
      // text displayed in the status bar when the notification (and the ticker) are updated, optional
      ticker: this.trackPlayed.track.titre
    })
    .then()
    .catch((error) => console.log(error) );

    this.musicControls.subscribe().subscribe(action => {

      const message = JSON.parse(action).message;
      switch (message) {
        case 'music-controls-next':
          // Do something
          break;
        case 'music-controls-previous':
          // Do something
          break;
        case 'music-controls-pause':
          // Do something
          this.pause(this.trackPlayed);
          break;
        case 'music-controls-play':
          // Do something
          this.play(this.trackPlayed);
          break;
        case 'music-controls-destroy':
          // Do something
          this.pause(this.trackPlayed);
          break;

        // External controls (iOS only)
        case 'music-controls-toggle-play-pause':
          // Do something
          break;
        case 'music-controls-seek-to':
          const seekToInSeconds = JSON.parse(action).position;
          this.musicControls.updateElapsed({
            elapsed: seekToInSeconds,
            isPlaying: true
          });
          // Do something
          this.trackPlayed.player.seekTo(seekToInSeconds*1000);
          break;
        case 'music-controls-skip-forward':
          // Do something
          let seekToInForward = (this.time + 15)*1000
          this.trackPlayed.player.seekTo(seekToInForward);
          break;
        case 'music-controls-skip-backward':
          // Do something
          let seekToInBackward = ((this.time - 15) >=  0) ? (this.time - 15)*1000 : 0;
          this.trackPlayed.player.seekTo(seekToInBackward);
          break;

        // Headset events (Android only)
        // All media button events are listed below
        case 'music-controls-media-button':
          // Do something
          break;
        case 'music-controls-headset-unplugged':
          // Do something
          this.pause(this.trackPlayed);        
          break;
        case 'music-controls-headset-plugged':
          // Do something
          break;
        default:
          break;
      }

    });

    this.musicControls.listen(); // activates the observable above
    this.musicControls.updateIsPlaying(true);
  }

  destroyMusicControls() {
    this.musicControls.destroy();
  }

  playAndPause(trackPlayed: TrackPlayed) {

    this.backgroundMode.enable();
    this.backgroundMode.setDefaults({ silent: true });

    if (this.plt.is('android')) {
        this.backgroundMode.on("activate").subscribe(()=>{
            this._store.dispatch(new trackPlayedAction.PlayOrPauseTrackPlayedAndroid(trackPlayed));
          });
        this._store.dispatch(new trackPlayedAction.PlayOrPauseTrackPlayedAndroid(trackPlayed));
    }
    else {
        this.backgroundMode.on("activate").subscribe(()=>{
            this._store.dispatch(new trackPlayedAction.PlayOrPauseTrackPlayedAndroid(trackPlayed));
          });
        this._store.dispatch(new trackPlayedAction.PlayOrPauseTrackPlayedIos(trackPlayed));
    }

  }

  play(trackPlayed: TrackPlayed) {
    this.backgroundMode.enable();
    this.backgroundMode.setDefaults({ silent: true });
    this.backgroundMode.on("activate").subscribe(()=>{
      this._store.dispatch(new trackPlayedAction.PlayTrackPlayed(trackPlayed));
    });
    this._store.dispatch(new trackPlayedAction.PlayTrackPlayed(trackPlayed));
  }

  pause(trackPlayed: TrackPlayed) {
    if (this.backgroundMode.isEnabled()) {
      this.backgroundMode.disable()
    }
    this._store.dispatch(new trackPlayedAction.PauseTrackPlayed(trackPlayed));
  }

}