import {Component, Input} from '@angular/core';


@Component({
  selector: 'progress-bar',
  templateUrl: './progress-bar.html'
})
export class ProgressBarComponent {
    @Input() player: any;

    public duration : number;    // Durée totale
    public time     : number;    // Temps écoulé
    public fraction : number;
    public percent  : number;

    public progressClass : string;

    constructor() {    
        this.time       = 0;
        this.duration   = 0;
    }

    ngOnInit(){

    this.player.onplay = () => {
      this.progressClass = "progress-bar progress-bar-striped progress-bar-animated";
    };

    this.player.onpause = () => {
      this.progressClass = "progress-bar progress-bar-striped";
    };
        
    this.player.ontimeupdate = () => {
      this.update();
    };
  }

  update () {
        this.duration = this.player.duration;    // Durée totale
        this.time     = this.player.currentTime; // Temps écoulé
        this.fraction = this.time / this.duration;
        this.percent  = Math.ceil(this.fraction * 100);
    }

    clickProgress(control, event) {

        let parent = this.getPosition(control);    // La position absolue de la progressBar
        let target = this.getMousePosition(event); // L'endroit de la progressBar où on a cliqué
    
        let x = target.x - parent.x; 

        let wrapperWidth = document.getElementById('progressBarControl').offsetWidth;
        
        let percent = Math.ceil((x / wrapperWidth) * 100);    
        let duration = this.player.duration;
        
        this.player.currentTime = (duration * percent) / 100;
    }

    getPosition(element){
        let top : number    = 0;
        let left : number   = 0;
        
        do {
            top  += element.offsetTop;
            left += element.offsetLeft;
        } while (element = element.offsetParent);
        
        return { x: left, y: top };
    }

    getMousePosition(event) {
        return {
            x: event.pageX,
            y: event.pageY
        };
    }

    formatTime(time) {
        let hours : any = Math.floor(time / 3600);
        let mins : any  = Math.floor((time % 3600) / 60);
        let secs : any  = Math.floor(time % 60);
        
        if (secs < 10) {
            secs = "0" + secs;
        }
        
        if (hours) {
            if (mins < 10) {
                mins = "0" + mins;
            }
            
            return hours + ":" + mins + ":" + secs; // hh:mm:ss
        } else {
            return mins + ":" + secs; // mm:ss
        }
    }

}