import { PlayerModalComponent } from './../player-modal/player-modal.component';
import { TrackPlayed } from './../../../models/track-played.model';
import { Component, OnInit, Input } from '@angular/core';
import { NavController, NavParams, LoadingController, ModalController, Platform, ViewController } from 'ionic-angular';
//model
import { Enseignement } from '../../../models/enseignement.model';
import { environment, getUrlAudio } from '../../../../environments/environment';
import { Theme } from './../../../models/theme.model';
//service
import { EnseignementService } from '../../../services/enseignement.service';
import { State, Store } from '@ngrx/store'
//reducer
import * as trackPlayedReducer from './../../../reducers/track-played/track-played.reducer';
import * as trackPlayedAction from './../../../reducers/track-played/track-played.actions';

//Cordova
import { Media, MediaObject } from '@ionic-native/media';

import { isNullOrUndefined } from "util";

import { AudioPlayerService } from '../audio-player.service';

@Component({
    selector: 'audio-track',
    templateUrl: './audio-track.html',
})
export class AudioTrackComponent implements OnInit {
    @Input() track: any;
    @Input() trackType: string;

    public player: MediaObject;
    public trackPlayed: TrackPlayed;
    public theme: Theme;

    public env: any;

    public urlSong: string;

    public modal: any;

    constructor(
        // private musicControls: MusicControls,
        private _enseignementService: EnseignementService,
        public audioPlayerService: AudioPlayerService,
        public modalCtrl: ModalController,
        private _store: Store<trackPlayedReducer.State>,
        private media: Media,
        public plt: Platform) {
        this.env = environment;
        this._store.select(trackPlayedReducer.getTrackPlayed).subscribe((data: TrackPlayed) => {
            this.trackPlayed = data;
        })

    }

    ngOnInit() {
        if (this.track.url !== '') {
            this.theme = new Theme(this.track.th_id,
                this.track.th_titre,
                this.track.th_description,
                this.track.th_image);

            if (this.plt.is('android')) {

                this.player = this.media.create(getUrlAudio(this.trackType, this.track));

                // to listen to plugin events:

                this.player.onStatusUpdate.subscribe(status => console.log(status)); // fires when file status changes

                this.player.onSuccess.subscribe(() => console.log('Action is successful'));

                this.player.onError.subscribe(error => console.log('Error!', error));

            }
        }
    }


    public countVues() {
        this._enseignementService
            .countVues(this.track.id)
            .subscribe(
            value => {
                this.track.vues = value.vues;
            },
            error => {
            });
    }

    playAndPause() {

        this.trackPlayed = new TrackPlayed(this.track, this.player, this.trackType);
        this.audioPlayerService.playAndPause(this.trackPlayed)
        this.openAudioDisplay(this.trackPlayed);
    }

    public openAudioDisplay(trackPlayed: TrackPlayed) {
        let modal = this.modalCtrl.create(PlayerModalComponent, trackPlayed);
        modal.onDidDismiss(() => this.audioPlayerService.initPlayer());
        modal.present();
    }

}