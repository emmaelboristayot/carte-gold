import { NavController, LoadingController, ModalController, Platform, ViewController } from 'ionic-angular';
import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store'

//model
import { AudioPlayer } from '../../models/audio-player.model';
import { Theme } from '../../models/theme.model';
import { environment } from '../../../environments/environment';

import { FilterByPipe } from 'ngx-pipes/src/app/pipes/array/filter-by';

//reducer
import * as trackPlayedReducer from './../../reducers/track-played/track-played.reducer';
import * as trackPlayedAction from './../../reducers/track-played/track-played.actions';

import * as adorationsReducer from './../../reducers/adorations/adorations.reducer';
import * as adorationsAction from './../../reducers/adorations/adorations.actions';

import { TrackPlayed } from '../../models/track-played.model';

import { PlayerModalComponent } from '../audio-player/player-modal/player-modal.component';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'adorations',
  templateUrl: './adorations.component.html',
  providers: [FilterByPipe]
})
export class AdorationsComponent implements OnInit {

  public adorations$: Observable<Array<AudioPlayer>>;

  public searchValue: string;
  public trackPlayed: TrackPlayed;
  public env: any;
  public readonly trackType: string = "ADORATION";

  constructor(
    private _store_track_played: Store<trackPlayedReducer.State>,
    private _store_adorations: Store<adorationsReducer.State>,
    public modalCtrl: ModalController,
    public navCtrl: NavController) {
    this.env = environment;

    this._store_track_played.select(trackPlayedReducer.getTrackPlayed).subscribe((data: TrackPlayed) => {
      this.trackPlayed = data;
    })

    this.adorations$ = this._store_adorations.pipe(select(adorationsReducer.selectAdorations$));

  }

  ngOnInit() {
    this.getAdorations();
  }
  
  getAdorations() {
    this._store_adorations.dispatch(new adorationsAction.LoadAdorations());
  }

  doRefresh(refresher) {
    this.getAdorations()

    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 3000);
  }

  openPlayer() {
    this.openAudioDisplay(this.trackPlayed);
  }

  public openAudioDisplay(trackPlayed: TrackPlayed) {
    let modal = this.modalCtrl.create(PlayerModalComponent, trackPlayed);
    modal.present();
  }

}
