import { NavController, NavParams, LoadingController, ModalController, Platform, ViewController } from 'ionic-angular';
import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store'

//model
import { Enseignement } from '../../models/enseignement.model';
import { Theme } from '../../models/theme.model';
import { environment } from '../../../environments/environment';

import { FilterByPipe } from 'ngx-pipes/src/app/pipes/array/filter-by';

//Reducers
import * as searchReducer from './../../reducers/search/search.reducer';
import * as searchActions from './../../reducers/search/search.actions';

//reducer
import * as trackPlayedReducer from './../../reducers/track-played/track-played.reducer';
import * as trackPlayedAction from './../../reducers/track-played/track-played.actions';

import { TrackPlayed } from '../../models/track-played.model';

import { PlayerModalComponent } from '../audio-player/player-modal/player-modal.component';

@Component({
  selector: 'theme-teaching',
  templateUrl: './theme-teaching.component.html',
  providers: [FilterByPipe]
})
export class ThemeTeachingComponent implements OnInit {
  public _enseignements: Array<Enseignement>;
  public searchValue: string;
  public trackPlayed: TrackPlayed;
  public env: any;
  public readonly trackType: string = "ENSEIGNEMENT";

  constructor(
    private _store_search: Store<searchReducer.State>,
    private _store_track_played: Store<trackPlayedReducer.State>,
    public modalCtrl: ModalController,
    public navCtrl: NavController,
    public params: NavParams) {
    this.env = environment;

    this._enseignements = params.data;

    this._store_search.select(searchReducer.getSearch).subscribe((data: string) => {
      this.searchValue = data;
    });

    this._store_track_played.select(trackPlayedReducer.getTrackPlayed).subscribe((data: TrackPlayed) => {
      this.trackPlayed = data;
    })

  }

  ngOnInit() {
    this._store_search.dispatch(new searchActions.SetSearch(""));
  }

  openPlayer() {
    this.openAudioDisplay(this.trackPlayed);
  }

  public openAudioDisplay(trackPlayed: TrackPlayed) {
    let modal = this.modalCtrl.create(PlayerModalComponent, trackPlayed);
    modal.present();
  }

}
