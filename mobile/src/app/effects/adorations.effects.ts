import { Injectable } from '@angular/core';
import { Action, Store } from '@ngrx/store';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { of } from 'rxjs/observable/of';
import { tap, map, exhaustMap, catchError, mergeMap } from 'rxjs/operators';
//Services
import { EnseignementService } from '../services/enseignement.service';

//model
import { AudioPlayer } from '../models/audio-player.model';

//Actions
import * as adorationsActions from './../reducers/adorations/adorations.actions';
import * as authAction from './../reducers/auth/auth.actions';


@Injectable()
export class AdorationsEffects {

    @Effect()
    load$ = this.actions$.pipe(
        ofType(adorationsActions.LOAD_ADORATIONS),
        mergeMap((action: adorationsActions.LoadAdorations) =>
            this.enseignementService
                .getAdorations().pipe(
                    map((adorations: AudioPlayer[]) => new adorationsActions.SetAdorations(adorations)),
                    catchError(error => of(new authAction.LoginRedirect()))
                )
        ));

    constructor(
        private actions$: Actions,
        private enseignementService: EnseignementService
    ) { }

}