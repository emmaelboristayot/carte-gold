import { Injectable, ViewChild } from '@angular/core';
import { Action, Store } from '@ngrx/store';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { of } from 'rxjs/observable/of';
import { tap, map, exhaustMap, catchError, mergeMap } from 'rxjs/operators';
import { Nav, Platform, App, NavController, AlertController, LoadingController } from 'ionic-angular';

//services
import { UserService } from '../services/user.service';
import { NetWorkService } from '../services/network.service';
//reducers
import * as userReducer from './../reducers/user/user.reducer';
import * as trackPlayedReducer from './../reducers/track-played/track-played.reducer';

//actions
import * as authAction from './../reducers/auth/auth.actions';
import * as userAction from './../reducers/user/user.actions';
import * as trackPlayedAction from './../reducers/track-played/track-played.actions';

//model
import { User, Authenticate } from './../models/user.model';
import { TrackPlayed } from './../models/track-played.model';

import { TabsPage } from '../components/tabs-page/tabs-page.component';
import { LoginPageComponent } from '../components/login-page/login-page.component';
//native
import { NativeStorage } from '@ionic-native/native-storage';
import { SplashScreen } from '@ionic-native/splash-screen';

@Injectable()
export class AuthEffects {

    @Effect()
    login$ = this.actions$.pipe(
        ofType(authAction.LOGIN),
        mergeMap((action: authAction.Login) => {
            this.loading();
            return this.userService
                .connexion(action.payload).pipe(
                map((user: User) => new authAction.LoginSuccess(user)),
                catchError(error => of(new authAction.LoginFailure(error)))
                )
            }

        ));

    @Effect()
    logout$ = this.actions$.pipe(
        ofType(authAction.LOGOUT),
        mergeMap((action: authAction.Logout) =>
            this.userService
                .deconnexion().pipe(
                map((user: User) => {
                    this.deleteLocalStorage();
                    this.releasePlayer();

                    //stop connexion watch
                    this._netWorkService.unsubscribe();

                    new authAction.LoginRedirect()
                    return new userAction.DeleteUser()
                }),
                catchError(error => {
                    //user not connected
                    this.deleteLocalStorage();
                    this.releasePlayer();
                    // Redirect the user
                    new authAction.LoginRedirect()

                    //stop connexion watch
                    this._netWorkService.unsubscribe();
                    return of(new authAction.LoginRedirect())
                })
                )
        ));

    @Effect()
    userSession$ = this.actions$.pipe(
        ofType(authAction.USERSESSION),
        mergeMap((action: authAction.UserSession) =>
            this.userService
                .quisuisje().pipe(
                map((user: User) => new authAction.LoginSuccess(user)),
                catchError(error => {
                    this.splashScreen.hide();
                    return of(new authAction.LoginRedirect())
                })
                )

        ));

    @Effect({ dispatch: false })
    loginSuccess$ = this.actions$.pipe(
        ofType(authAction.LOGINSUCCESS),
        tap((action: authAction.LoginSuccess) => {
            this.navCtrl = this.app.getRootNav();
            this._store.dispatch(new userAction.SetUser(action.payload));
            this.nativeStorage.setItem('user', action.payload)
                .then(() => console.log('Stored item!'),
                error => console.error('Error storing item', error));
            this.loader.dismiss();
            this.splashScreen.hide();
            this.navCtrl.setRoot(TabsPage);
        }));

    @Effect({ dispatch: false })
    loginRedirect$ = this.actions$.pipe(
        ofType(authAction.LOGINREDIRECT, authAction.LOGOUT),
        tap(() => {
            this.navCtrl = this.app.getRootNav();
            this.navCtrl.setRoot(LoginPageComponent)
        })
    );

    @Effect({ dispatch: false })
    loginFailure$ = this.actions$.pipe(
        ofType(authAction.LOGINFAILURE),
        tap(() => {
            this.loader.dismiss();
            this.showErrorMessage()
        })
    );

    private navCtrl: NavController;
    public trackPlayed: TrackPlayed;
    public loader: any;

    constructor(
        private actions$: Actions,
        private userService: UserService,
        private _store: Store<userReducer.State>,
        private _store_track_played: Store<trackPlayedReducer.State>,
        private app: App,
        public alertCtrl: AlertController,
        private _netWorkService: NetWorkService,
        private nativeStorage: NativeStorage,
        public splashScreen: SplashScreen,
        public loadingCtrl: LoadingController
    ) {
        this._store_track_played.select(trackPlayedReducer.getTrackPlayed).subscribe((data: TrackPlayed) => {
            this.trackPlayed = data;
        })
        
        this.loader = this.loadingCtrl.create({
            content: "Chargement..."
        });
    }

    showErrorMessage() {
        let alert = this.alertCtrl.create({
            title: 'Erreur',
            subTitle: 'Identifiant ou mot de passe incorrect !',
            buttons: ['OK']
        });
        alert.present();
    }

    deleteLocalStorage() {
        this.nativeStorage.remove('user');
        this.nativeStorage.remove('authenticate');
        this.nativeStorage.remove('enseignements');
        this.nativeStorage.remove('themes');
    }

    releasePlayer() {
        if (this.trackPlayed && this.trackPlayed.player) {
            this.trackPlayed.player.stop();
            this.trackPlayed.player.release();
        }
    }

    loading() {
        this.loader.present();
    }

}