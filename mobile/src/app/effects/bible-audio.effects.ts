import { Injectable } from '@angular/core';
import { Action, Store } from '@ngrx/store';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { of } from 'rxjs/observable/of';
import { tap, map, exhaustMap, catchError, mergeMap } from 'rxjs/operators';
//Services
import { EnseignementService } from '../services/enseignement.service';

//model
import { BibleAudio } from './../models/bible-audio.model';

//Actions
import * as bibleAudioActions from './../reducers/bible-audio/bible-audio.actions';
import * as authAction from './../reducers/auth/auth.actions';


@Injectable()
export class BibleAudioEffects {

    @Effect()
    load$ = this.actions$.pipe(
        ofType(bibleAudioActions.LOAD_BIBLE),
        mergeMap((action: bibleAudioActions.LoadBibleAudio) =>
            this.enseignementService
                .getBibleAudio().pipe(
                    map((bibleAudio: BibleAudio[]) => new bibleAudioActions.SetBibleAudio(bibleAudio)),
                    catchError(error => of(new authAction.LoginRedirect()))
                )
        ));

    constructor(
        private actions$: Actions,
        private enseignementService: EnseignementService
    ) { }

}