import { AudioPlayer } from "./audio-player.model";

export class BibleAudio extends AudioPlayer{

  constructor(
    public theme:string,
    public type:string
    ) { 
      super();
    }

}
