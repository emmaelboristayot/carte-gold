import { Abonnement } from "./abonnement.model";

export interface Authenticate {
  email: string;
  password: string;
}

export class User{
  
  constructor(
    public id: number,
    public nom:string,
    public prenom:string,
    public email:string,
    public telephone:string,
    public password:string,
    public created_at:string,
    public updated_at:string,
    public is_star: number,
    public abonnements: Abonnement
    ) { }

}
