//Cordova
import { Media, MediaObject } from '@ionic-native/media';

export class TrackPlayed{

  constructor(
    public track: any,
    public player:MediaObject,
    public readonly trackType: string
    ) { }

}
