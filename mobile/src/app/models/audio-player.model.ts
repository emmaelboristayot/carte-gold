export class AudioPlayer{
  public id: number;
  public titre:string;
  public soustitre:string;
  public url:string;
  public resume:string;
  public image:string;
  public date:string;
  public vues: number;
  
  constructor() { }

}
