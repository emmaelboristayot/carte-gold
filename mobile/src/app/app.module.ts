import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule, LOCALE_ID } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { AudioPlayerModule } from './components/audio-player/audio-player.module';
//Components
import { MyApp } from './app.component';
import { TeachingPageComponent } from './components/teaching-page/teaching-page.component';
import { ThemeCardComponent } from './components/teaching-page/theme-card/theme-card.component';
import { ThemeTeachingComponent } from './components/theme-teaching/theme-teaching.component';
import { PlayerModalComponent } from './components/audio-player/player-modal/player-modal.component';
import { LoginPageComponent } from './components/login-page/login-page.component';
import { BibleAudioComponent } from './components/bible-audio/bible-audio.component';
import { AdorationsComponent } from './components/adorations/adorations.component';
//Native
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

//Service
import { EnseignementService } from './services/enseignement.service';
import { UserService } from './services/user.service';
import { CommonService } from './components/audio-player/common.service';
import { AudioPlayerService } from './components/audio-player/audio-player.service';
import { NetWorkService } from './services/network.service';

//Modules
import { HttpModule } from '@angular/http';
//Reducer
import { StoreModule } from '@ngrx/store';
import { reducers } from './reducers';

//Filter
import {NgPipesModule} from 'ngx-pipes';
//Cordova
import { Media, MediaObject } from '@ionic-native/media';
import { MusicControls } from '@ionic-native/music-controls';
import { Network } from '@ionic-native/network';
import { NativeStorage } from '@ionic-native/native-storage';
import { BackgroundMode } from '@ionic-native/background-mode';
//pages
import { TabsPage } from './components/tabs-page/tabs-page.component';
//Effects
import { EffectsModule } from '@ngrx/effects';
import { AuthEffects } from './effects/auth.effects';
import { EnseignementsEffects } from './effects/enseignements.effects';
import { BibleAudioEffects } from './effects/bible-audio.effects';
import { AdorationsEffects } from './effects/adorations.effects';
//Language FR
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';

// the second parameter 'fr' is optional
registerLocaleData(localeFr, 'fr');


@NgModule({
  declarations: [
    LoginPageComponent,
    MyApp,
    TeachingPageComponent,
    ThemeCardComponent,
    ThemeTeachingComponent,
    BibleAudioComponent,
    AdorationsComponent,
    TabsPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    NgPipesModule,
    AudioPlayerModule,
    IonicModule.forRoot(MyApp),
    StoreModule.forRoot(reducers),
    EffectsModule.forRoot([AuthEffects, EnseignementsEffects, BibleAudioEffects, AdorationsEffects]),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    LoginPageComponent,
    MyApp,
    TeachingPageComponent,
    ThemeTeachingComponent,
    PlayerModalComponent,
    BibleAudioComponent,
    AdorationsComponent,
    TabsPage
  ],
  providers: [
    { provide: LOCALE_ID, useValue: "FR" },
    BackgroundMode,
    NativeStorage,
    MusicControls,
    Network,
    Media,
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    UserService,
    EnseignementService,
    CommonService,
    AudioPlayerService,
    NetWorkService
  ]
})
export class AppModule {}
