import { EnseignementConstant } from './../../constants/enseignement.constant';
import { TrackPlayed } from './../../models/track-played.model';
import { ActionReducer } from '@ngrx/store';
import { isNullOrUndefined } from "util";
import * as TrackPlayedActions from './track-played.actions';
import { environment, getUrlAudio } from '../../../environments/environment';
//Cordova
import { Media, MediaObject } from '@ionic-native/media';
import { Platform } from 'ionic-angular';

export type Action = TrackPlayedActions.All;

export interface State {
    trackPlayed: TrackPlayed;
}

export function reducer(state: TrackPlayed, action: Action) {
    switch (action.type) {

        case TrackPlayedActions.SET_TRACK_PLAYED:
            state = action.payload;
            return state;

        case TrackPlayedActions.PLAY_TRACK_PLAYED:
            state.player.play();
            return state;

        case TrackPlayedActions.PAUSE_TRACK_PLAYED:
            state.player.pause();
            return state;

        case TrackPlayedActions.PLAY_OR_PAUSE_TRACK_PLAYED_IOS:
            state = (state) ? state : { track: EnseignementConstant.DEFAULT, player: null, trackType: '' }
            //ios
            if (action.payload.track.url === state.track.url) {
                return state;
            }

            let player = new Media().create(getUrlAudio(action.payload.trackType, action.payload.track));

            player.play();
            action.payload.player = player;
            state = action.payload;

            //ios

            return state;

        case TrackPlayedActions.PLAY_OR_PAUSE_TRACK_PLAYED_ANDROID:

            state = (state) ? state : { track: EnseignementConstant.DEFAULT, player: null, trackType: '' }

            if (state.player !== action.payload.player && !isNullOrUndefined(state.player)) {
                state.player.pause();
            }
            state = action.payload;
            state.player.play();

            return state;

        default:
            return state;
    }
}

export const getTrackPlayed = (state: State) => state.trackPlayed;