import { User } from './../../models/user.model';
import { Action } from '@ngrx/store';

export const SET_USER  = '[User] set';
export const GET_USER  = '[User] get';
export const DELETE_USER  = '[User] delete';

export class SetUser implements Action {
  readonly type = SET_USER;
  constructor(public payload: User) {}
}

export class GetUser implements Action {
  readonly type = GET_USER;
}

export class DeleteUser implements Action {
  readonly type = DELETE_USER;
}

export type All
  = SetUser
  | GetUser
  | DeleteUser;