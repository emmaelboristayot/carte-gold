import { createSelector } from '@ngrx/store';
import * as BibleAudioActions from './bible-audio.actions';
import { BibleAudio } from '../../models/bible-audio.model';

export type Action = BibleAudioActions.All;

export interface State {
    bibleAudio : BibleAudio[];
}

const loadState = () => { 
    try {
        const serializedState = localStorage.getItem('bibleAudio')
        if( serializedState === null){
            return 
        }
        return JSON.parse(serializedState)
    } catch (err) {
        return 
    }
}
const saveState = (state) => {
    try {
        const serializedState = JSON.stringify(state)
        localStorage.setItem('bibleAudio', serializedState)
        return state
    } catch (err) {
        // Ignore err
    }
}

export function reducer(state = loadState(), action: Action) {
   switch (action.type) {
      case BibleAudioActions.SET_BIBLE:
         state = saveState(action.payload);
         return state;

      case BibleAudioActions.GET_BIBLE:
         return state;

      default:
         return state;
   }
}

export const getBibleAudio$ = (state: State) => state.bibleAudio;

export const selectBibleAudio$ =
createSelector(getBibleAudio$,(bibleAudio) => bibleAudio);