import { Action } from '@ngrx/store';
import { BibleAudio } from '../../models/bible-audio.model';

export const SET_BIBLE  = '[BibleAudio] set';
export const GET_BIBLE  = '[BibleAudio] get';
export const LOAD_BIBLE  = '[BibleAudio] load';

export class SetBibleAudio implements Action {
  readonly type = SET_BIBLE;
  constructor(public payload: BibleAudio[]) {}
}

export class GetBibleAudio implements Action {
  readonly type = GET_BIBLE;
}

export class LoadBibleAudio implements Action {
  readonly type = LOAD_BIBLE;
}

export type All
  = SetBibleAudio
  | GetBibleAudio
  | LoadBibleAudio;