import { Action } from '@ngrx/store';
import { AudioPlayer } from '../../models/audio-player.model';

export const SET_ADORATIONS  = '[Adorations] set';
export const GET_ADORATIONS  = '[Adorations] get';
export const LOAD_ADORATIONS  = '[Adorations] load';

export class SetAdorations implements Action {
  readonly type = SET_ADORATIONS;
  constructor(public payload: AudioPlayer[]) {}
}

export class GetAdorations implements Action {
  readonly type = GET_ADORATIONS;
}

export class LoadAdorations implements Action {
  readonly type = LOAD_ADORATIONS;
}

export type All
  = SetAdorations
  | GetAdorations
  | LoadAdorations;