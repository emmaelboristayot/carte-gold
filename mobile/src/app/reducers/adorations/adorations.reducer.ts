import { createSelector } from '@ngrx/store';
import * as AdorationsActions from './adorations.actions';
import { AudioPlayer } from '../../models/audio-player.model';

export type Action = AdorationsActions.All;

export interface State {
    adorations : AudioPlayer[];
}

const loadState = () => { 
    try {
        const serializedState = localStorage.getItem('adorations')
        if( serializedState === null){
            return 
        }
        return JSON.parse(serializedState)
    } catch (err) {
        return 
    }
}
const saveState = (state) => {
    try {
        const serializedState = JSON.stringify(state)
        localStorage.setItem('adorations', serializedState)
        return state
    } catch (err) {
        // Ignore err
    }
}

export function reducer(state = loadState(), action: Action) {
   switch (action.type) {
      case AdorationsActions.SET_ADORATIONS:
         state = saveState(action.payload);
         return state;

      case AdorationsActions.GET_ADORATIONS:
         return state;

      default:
         return state;
   }
}

export const getAdorations$ = (state: State) => state.adorations;

export const selectAdorations$ =
createSelector(getAdorations$,(adorations) => adorations);