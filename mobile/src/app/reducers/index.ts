import { ActionReducerMap } from '@ngrx/store';

import * as enseignementsReducer from './enseignements/enseignements.reducer';
import * as userReducer from './user/user.reducer';
import * as searchReducer from './search/search.reducer';
import * as themesReducer from './themes/themes.reducer';
import * as trackPlayedReducer from './track-played/track-played.reducer';
import * as authReducer from './auth/auth.reducer';
import * as bibleAudioReducer from './bible-audio/bible-audio.reducer';
import * as adorationsReducer from './adorations/adorations.reducer';

import { Enseignement } from './../models/enseignement.model';
import { Theme } from './../models/theme.model';
import { TrackPlayed } from './../models/track-played.model';

export interface State {
    enseignements: enseignementsReducer.State,
    user: userReducer.State,
    search: searchReducer.State,
    trackPlayed: TrackPlayed,
    themes: themesReducer.State,
    auth: authReducer.State,
    bibleAudio: bibleAudioReducer.State,
    adorations: adorationsReducer.State
}

export const reducers: ActionReducerMap<State> = {
    enseignements: enseignementsReducer.reducer,
    user: userReducer.reducer,
    search: searchReducer.reducer,
    trackPlayed: trackPlayedReducer.reducer,
    themes: themesReducer.reducer,
    auth: authReducer.reducer,
    bibleAudio: bibleAudioReducer.reducer,
    adorations: adorationsReducer.reducer
};