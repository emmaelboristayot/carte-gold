import { Theme } from './../../models/theme.model';
import { Action } from '@ngrx/store';

export const SET_THEMES  = '[Themes] set';
export const GET_THEMES  = '[Themes] get';
export const LOAD_THEMES  = '[Themes] load';

export class SetThemes implements Action {
  readonly type = SET_THEMES;
  constructor(public payload: Theme[]) {}
}

export class GetThemes implements Action {
  readonly type = GET_THEMES;
}

export class LoadThemes implements Action {
  readonly type = LOAD_THEMES;
}

export type All
  = SetThemes
  | GetThemes
  | LoadThemes;