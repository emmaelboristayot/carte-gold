import { createSelector } from '@ngrx/store';
import { Theme } from './../../models/theme.model';
import * as ThemesActions from './themes.actions';

export type Action = ThemesActions.All;

export interface State {
    themes : Theme[];
}

const loadState = () => { 
    try {
        const serializedState = localStorage.getItem('themes')
        if( serializedState === null){
            return 
        }
        return JSON.parse(serializedState)
    } catch (err) {
        return 
    }
}
const saveState = (state) => {
    try {
        const serializedState = JSON.stringify(state)
        localStorage.setItem('themes', serializedState)
        return state
    } catch (err) {
        // Ignore err
    }
}

export function reducer(state = loadState(), action: Action) {
   switch (action.type) {
      case ThemesActions.SET_THEMES:
         state = saveState(action.payload);
         return state;

      case ThemesActions.GET_THEMES:
         return state;

      default:
         return state;
   }
}

export const getThemes$ = (state: State) => state.themes;

export const selectThemes$ =
createSelector(getThemes$,(themes) => themes);