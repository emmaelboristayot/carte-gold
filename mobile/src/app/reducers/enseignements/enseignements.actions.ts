import { Enseignement } from './../../models/enseignement.model';
import { Action } from '@ngrx/store';

export const SET_ENSEIGNEMENTS  = '[Enseignements] set';
export const GET_ENSEIGNEMENTS  = '[Enseignements] get';
export const LOAD_ENSEIGNEMENTS  = '[Enseignements] load';

export class SetEnseignements implements Action {
  readonly type = SET_ENSEIGNEMENTS;
  constructor(public payload: Enseignement[]) {}
}

export class GetEnseignements implements Action {
  readonly type = GET_ENSEIGNEMENTS;
}

export class LoadEnseignements implements Action {
  readonly type = LOAD_ENSEIGNEMENTS;
}

export type All
  = SetEnseignements
  | GetEnseignements
  | LoadEnseignements;