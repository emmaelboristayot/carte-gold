import { createSelector } from '@ngrx/store';
import { Enseignement } from './../../models/enseignement.model';
import * as EnseignementsActions from './enseignements.actions';

export type Action = EnseignementsActions.All;

export interface State {
    enseignements : Enseignement[];
}

const loadState = () => {
    try {
        const serializedState = localStorage.getItem('enseignements')
        if (serializedState === null) {
            return
        }
        return JSON.parse(serializedState)
    } catch (err) {
        return
    }
}
const saveState = (state) => {
    try {
        const serializedState = JSON.stringify(state)
        localStorage.setItem('enseignements', serializedState)
        return state
    } catch (err) {
        // Ignore err
    }
}

export function reducer(state = loadState(), action: Action) {
    switch (action.type) {
        case EnseignementsActions.SET_ENSEIGNEMENTS:
            state = saveState(action.payload);
            return state;

        case EnseignementsActions.GET_ENSEIGNEMENTS:
            return state;

        default:
            return state;
    }
}

export const getEnseignements$ = (state: State) => state.enseignements;

export const selectEnseignements$ =
createSelector(getEnseignements$,(enseignements) => enseignements);