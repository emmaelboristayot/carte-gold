import { Action } from '@ngrx/store';
import { User, Authenticate } from './../../models/user.model';

export const LOGIN          = '[Auth] Login';
export const LOGOUT         = '[Auth] Logout';
export const USERSESSION    = '[Auth] User Session';
export const LOGINSUCCESS   = '[Auth] Login Success';
export const LOGINFAILURE   = '[Auth] Login Failure';
export const LOGINREDIRECT  = '[Auth] Login Redirect';


export class Login implements Action {
  readonly type = LOGIN;

  constructor(public payload: Authenticate) {}
}

export class UserSession implements Action {
  readonly type = USERSESSION;
}

export class LoginSuccess implements Action {
  readonly type = LOGINSUCCESS;

  constructor(public payload: User ) {}
}

export class LoginFailure implements Action {
  readonly type = LOGINFAILURE;

  constructor(public payload: any) {}
}

export class LoginRedirect implements Action {
  readonly type = LOGINREDIRECT;
}

export class Logout implements Action {
  readonly type = LOGOUT;
}

export type AuthActions =
  | Login
  | UserSession
  | LoginSuccess
  | LoginFailure
  | LoginRedirect
  | Logout;