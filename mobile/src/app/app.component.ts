import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';

import { TeachingPageComponent } from './components/teaching-page/teaching-page.component';
import { NetWorkService } from './services/network.service';

import { NativeStorage } from '@ionic-native/native-storage';
import { User } from './models/user.model';
import { Store } from '@ngrx/store'

//reducer
import * as userReducer from './reducers/user/user.reducer';
import * as userAction from './reducers/user/user.actions';

import * as authReducer from './reducers/auth/auth.reducer';
import * as authAction from './reducers/auth/auth.actions';

//pages
import { TabsPage } from './components/tabs-page/tabs-page.component';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any;
  isLoggedIn: boolean = false;
  user: User;
  abonnements = [];

  pages: Array<{ title: string, component: any }>;

  constructor(public platform: Platform,
    public statusBar: StatusBar,
    private _netWorkService: NetWorkService,
    private _store: Store<userReducer.State>,
    private _store_auth: Store<authReducer.State>) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Accueil', component: TeachingPageComponent }
    ];

    this._store.select(userReducer.getUser).subscribe((data : User) => {
      this.user = data;
      if (data && data.abonnements && data.abonnements.abonnements) {
      this.isLoggedIn = true;
      this.getAbonnement(this.user.abonnements.abonnements);
      }
    });

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();

      this._netWorkService.initNetworkDetection();

      this._store.dispatch(new authAction.UserSession());

    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }

  deconnexion() {
    this._store.dispatch(new authAction.Logout());
  }

  getAbonnement(list: string) {
    let yearToStart = 2014;
    this.abonnements = []

    list.split(',').forEach(element => {
      let year = yearToStart + parseInt(element);
      this.abonnements.push(year);
    });

  }

  ngOnDestroy() {
    console.log("destroy app");
 }

}
