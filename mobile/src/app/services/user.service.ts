// Observable Version
import { Injectable }              from '@angular/core';
import { Http, Response }          from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';

//model
import { User } from './../models/user.model';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

import { environment } from '../../environments/environment';

@Injectable()
export class UserService {
  private connexionUrl = `${environment.api}/connexion`;  // URL to web API
  private inscriptionUrl = `${environment.api}/inscription`;  // URL to web API
  private modificationUrl = `${environment.api}/modification`;  // URL to web API
  private changepasswordUrl = `${environment.api}/change-password`;  // URL to web API
  private quisuisjeUrl = `${environment.api}/quisuisje`;  // URL to web API
  private deconnexionUrl = `${environment.api}/deconnexion`;  // URL to web API

  public isLoggedIn : boolean = false;

  // store the URL so we can redirect after logging in
  public redirectUrl: string;

  constructor (private http: Http) {}

  connexion(user: any): Observable<User> {
    let headers = new Headers({ 'Content-Type':  'application/x-www-form-urlencoded; charset=UTF-8' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });

    return this.http.post(this.connexionUrl, user, options)
                    .map((res:Response) => {
                      this.isLoggedIn = ( res.json().code) ? false : true;
                      return res.json();
                    })
                    .catch(this.handleError);
  }

  inscription(inscrit: any): Observable<any> {
    let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });

    return this.http.post(this.inscriptionUrl, inscrit, options)
                    .map((res:Response) => res.json())
                    .catch(this.handleError);
  }

  modification(inscrit: any): Observable<any> {
    let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });

    return this.http.post(this.modificationUrl, inscrit, options)
                    .map((res:Response) => res.json())
                    .catch(this.handleError);
  }

  changePassword(inscrit: any): Observable<any> {
    let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });

    return this.http.post(this.changepasswordUrl, inscrit, options)
                    .map((res:Response) => res.json())
                    .catch(this.handleError);
  }

  deconnexion(): Observable<any> {
    let headers = new Headers({ 'Content-Type':  'application/x-www-form-urlencoded; charset=UTF-8' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });

    return this.http.get(this.deconnexionUrl, options)
                    .map((res:Response) => {
                      this.isLoggedIn = ( res.json().user === 'logout') ? false : true;
                      return res.json();
                    })
                    .catch(this.handleError);
  }

  quisuisje(): Observable<any> {
    let headers = new Headers({ 'Content-Type':  'application/x-www-form-urlencoded; charset=UTF-8' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });

    return this.http.get(this.quisuisjeUrl, options)
                    .map((res:Response) => {
                      this.isLoggedIn = ( res.json().code) ? false : true;
                      return res.json();
                    })
                    .catch(this.handleError);
  }

  private handleError (error: Response | any) {
    // In a real world app, you might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

}