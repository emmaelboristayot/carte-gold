import { AbstractControl } from '@angular/forms';


export const passwordMatcher = (control: AbstractControl): { [key: string]: boolean } => {
  if (control.parent != undefined) {
    const password = control.parent.get('password').value;
    const confirm = control.parent.get('confirm').value;
    if (!password || !confirm) {
      return null;
    }
    return password === confirm ? null : { nomatch: true };
  }
  return null;

};