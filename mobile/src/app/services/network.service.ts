// Observable Version
import { Injectable } from '@angular/core';
import { Network } from '@ionic-native/network';
import { ToastController } from 'ionic-angular';

@Injectable()
export class NetWorkService {
  //connexion internet
  private connectSubscription: any;
  public isConnected: boolean;

  constructor(private network: Network, public toastCtrl: ToastController) {
  }

  initNetworkDetection() {
    this.connectSubscription = this.network.onchange().subscribe((value: any) => {

      if (value.type === 'offline') {
        this.isConnected = false;

        let toast = this.toastCtrl.create({
          message: 'vous êtes hors ligne !',
          duration: 3000,
          position: 'top'
        });
        toast.present();
      } else {
        this.isConnected = false;
      }

    },
      error => {
      }
    );
  }

  unsubscribe() {
    this.connectSubscription.unsubscribe();
  }

}