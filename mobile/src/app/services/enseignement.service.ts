// Observable Version
import { Injectable }              from '@angular/core';
import {Http, Response, ResponseContentType} from '@angular/http';
import { Headers, RequestOptions } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

import { environment } from '../../environments/environment';

//model
import { Enseignement } from '../models/enseignement.model';
import { Theme } from './../models/theme.model';
import { BibleAudio } from './../models/bible-audio.model';

@Injectable()
export class EnseignementService {
  private getEnseignementsUrl = `${environment.api}/player/enseignements`;  // URL to web API
  private getAdorationsUrl = `${environment.api}/player/adorations`;  // URL to web API
  private getBibleAudioUrl = `${environment.api}/player/bible-audio`;  // URL to web API
  private getThemesUrl = `${environment.api}/player/themes`;  // URL to web API
  private getFavorisUrl = `${environment.api}/player/favoris`;  // URL to web API

  constructor (private http: Http) {}

  getEnseignements(): Observable<Enseignement[]> {
    let headers = new Headers({ 'Content-Type':  'application/x-www-form-urlencoded; charset=UTF-8' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    
    return this.http.get(this.getEnseignementsUrl, options)
                    .map((res:Response) => {
                      return res.json();
                    })
                    .catch(this.handleError);
  }

  getAdorations(): Observable<Enseignement[]> {
    let headers = new Headers({ 'Content-Type':  'application/x-www-form-urlencoded; charset=UTF-8' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    
    return this.http.get(this.getAdorationsUrl, options)
                    .map((res:Response) => {
                      return res.json();
                    })
                    .catch(this.handleError);
  }

  getBibleAudio(): Observable<BibleAudio[]> {
    let headers = new Headers({ 'Content-Type':  'application/x-www-form-urlencoded; charset=UTF-8' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    
    return this.http.get(this.getBibleAudioUrl, options)
                    .map((res:Response) => {
                      return res.json();
                    })
                    .catch(this.handleError);
  }

  getFavoris(): Observable<Enseignement[]> {
    let headers = new Headers({ 'Content-Type':  'application/x-www-form-urlencoded; charset=UTF-8' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    
    return this.http.get(this.getFavorisUrl, options)
                    .map((res:Response) => {
                      return res.json();
                    })
                    .catch(this.handleError);
  }

  ajouterFavoi(idTrack: number): Observable<any> {
    let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' });
    let options = new RequestOptions({ headers: headers });
    let ajouterFavoiUrl = `${environment.api}/favoris/${idTrack}/supprimer-favori`;  // URL to web API

    return this.http.get(ajouterFavoiUrl, options)
                    .map((res:Response) => res.json())
                    .catch(this.handleError);
  }

  supprimerFavori(idTrack: number): Observable<any> {
    let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' });
    let options = new RequestOptions({ headers: headers });
    let supprimerFavoriUrl = `${environment.api}/favoris/${idTrack}/ajouter-favori`;  // URL to web API

    return this.http.get(supprimerFavoriUrl, options)
                    .map((res:Response) => res.json())
                    .catch(this.handleError);
  }

  getThemes(): Observable<Theme[]> {
    let headers = new Headers({ 'Content-Type':  'application/x-www-form-urlencoded; charset=UTF-8' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    
    return this.http.get(this.getThemesUrl, options)
                    .map((res:Response) => {
                      return res.json();
                    })
                    .catch(this.handleError);
  }

  countVues(id : number): Observable<Enseignement>  {
    let headers = new Headers({ 'Content-Type':  'application/x-www-form-urlencoded; charset=UTF-8' });
    let options = new RequestOptions({ headers: headers, withCredentials: true });
    let url = `${environment.api}/player/count-vues/${id}`;  // URL to web API
    return this.http.get(url, options)
                    .map((res:Response) => {
                      return res.json();
                    })
                    .catch(this.handleError);
  }

    downloadEnseignementByUrl(url : string): Observable<Blob> {
        let headers = new Headers({ 'Content-Type':  'application/x-www-form-urlencoded; charset=UTF-8' });
        let options = new RequestOptions({ headers: headers, withCredentials: true, responseType: ResponseContentType.Blob });

        return this.http.get(url, options)
            .map((res:Response) => {
                return res.json();
            })
            .catch(this.handleError);
    }


  private handleError (error: Response | any) {
    // In a real world app, you might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

}