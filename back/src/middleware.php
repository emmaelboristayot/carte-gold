<?php
// Application middleware

$app->add(new App\guard\RequestGuard);

// Enable CORS
$app->add(new App\guard\Cors());