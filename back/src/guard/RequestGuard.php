<?php
namespace App\guard;

use Interop\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class RequestGuard 
{
   
   public function __invoke($request, $response, $next) {
    $route = $request->getAttribute('route');

    // return NotFound for non existent route
    if (empty($route)) {
        return $response->withStatus(404)
                ->withHeader('Content-Type', 'text/html')
                ->write('Page not found');
    }

    $name = $route->getName();
    
    if (!isset($_SESSION['user']) && $name !== "connexion") {
      return $response->withJson([
            'permission' => 'denied',
            'description' => 'user not authenticated',
            'code' => 300
        ])->withStatus(401);
    }
    else {
        return $next($request, $response);
    }

   }
   
}