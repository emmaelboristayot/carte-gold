<?php
namespace App\guard;

use Interop\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class Cors 
{
   
   public function __invoke($request, $response, $next) {
    
    $response = $next($request, $response);

    return $response
    ->withHeader('Access-Control-Allow-Origin', 'https://cartegold.lanormandiepourchrist.com')
    ->withHeader('Access-Control-Allow-Credentials', 'true')
    ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Origin, Cache-Control, Pragma, Authorization, Accept, Accept-Encoding')
    ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
    ->withHeader('Access-Control-Max-Age', '1000');

   }
   
}