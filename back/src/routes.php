<?php
// Routes

$app->group('/user', function () {
    $this->post('/connexion', App\controllers\UserController::class . ':connexion')->setName('connexion');
    $this->get('/deconnexion', App\controllers\UserController::class . ':deconnexion')->setName('deconnexion');
    $this->post('/inscription', App\controllers\UserController::class . ':inscription')->setName('inscription');
    $this->post('/modification', App\controllers\UserController::class . ':modification')->setName('modification');
    $this->post('/change-password', App\controllers\UserController::class . ':changePassword')->setName('change-password');
    $this->get('/quisuisje', App\controllers\UserController::class . ':quisuisje')->setName('quisuisje');

    $this->group('/player', function () {

        $this->get('/abonnements', App\controllers\EnseignementController::class . ':abonnements')->setName('abonnements');
        $this->get('/themes', App\controllers\EnseignementController::class . ':themes')->setName('themes');
        $this->post('/auteurs', App\controllers\EnseignementController::class . ':auteurs')->setName('auteurs');
        $this->get('/enseignements', App\controllers\EnseignementController::class . ':enseignements')->setName('enseignements');
        $this->get('/adorations', App\controllers\EnseignementController::class . ':adorations')->setName('adorations');
        $this->get('/bible-audio', App\controllers\EnseignementController::class . ':bibleAudio')->setName('bible-audio');        
        $this->get('/count-vues/{id_enseignements}', App\controllers\EnseignementController::class . ':countVues')->setName('count-vue');

        $this->group('/historique', function () {
            $this->post('/en-cours', App\controllers\PlayerController::class . ':enCours')->setName('en-cours');
            $this->post('/ecoutes', App\controllers\PlayerController::class . ':ecoutes')->setName('ecoutes');
            $this->post('/save-en-cours/{id_enseignements}', App\controllers\PlayerController::class . ':saveEnCours')->setName('save-en-cours');
            $this->post('/add-ecoutes/{id_enseignements}', App\controllers\PlayerController::class . ':addEcoutes')->setName('add-ecoutes');
        });

        $this->get('/favoris', App\controllers\FavoriController::class . ':favoris')->setName('favoris');
        $this->group('/favoris/{id_enseignements}', function () {
            $this->get('/ajouter-favori', App\controllers\FavoriController::class . ':ajouterFavori')->setName('ajouter-favori');
            $this->get('/supprimer-favori', App\controllers\FavoriController::class . ':supprimerFavori')->setName('supprimer-favori');
        });

        $this->post('/notes', App\controllers\NoteController::class . ':notes')->setName('notes');
        $this->group('/notes/{id_enseignements}', function () {
            $this->post('/ajouter-note', App\controllers\NoteController::class . ':ajouterNote')->setName('ajouter-note');
            $this->post('/supprimer-note', App\controllers\NoteController::class . ':supprimerNote')->setName('supprimer-note');
            $this->post('/modifier-note', App\controllers\NoteController::class . ':modifierNote')->setName('modifier-note');
        });

        
        $this->group('/analyses-ressources', function () {
            $this->post('/similaires-au-dernier', App\controllers\RessourcesAnalysisController::class . ':similairesAuDernier')->setName('similaires-au-dernier');
            $this->post('/les-plus-regardes', App\controllers\RessourcesAnalysisController::class . ':lesPlusRegardes')->setName('les-plus-regardes');
            $this->post('/les-nouvautes', App\controllers\RessourcesAnalysisController::class . ':lesNouvautes')->setName('les-nouvautes');
            $this->post('/sugestions-definies', App\controllers\RessourcesAnalysisController::class . ':sugestionsDefinies')->setName('sugestions-definies');
            $this->post('/les-plus-aimes', App\controllers\RessourcesAnalysisController::class . ':lesPlusAimes')->setName('les-plus-aimes');
        });

    });

    
});





