<?php
namespace App\controllers;

use Interop\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

use App\models\Enseignement;
use App\models\Theme;
use App\models\Abonnement;
use App\models\Auteur;
use App\models\Player;
use App\models\Favori;

class RessourcesAnalysisController 
{
   protected $container;

   // constructor receives container instance
   public function __construct(ContainerInterface $container) {
       $this->container = $container;
   }

   public function sugestionParAnalyse($request, $response, $args) {

        return $response;
   }

   public function sugestionsDefinies($request, $response, $args) {

        $abonnements        = explode(",", $_SESSION['abonnements']->abonnements);

        $enseignements      = Enseignement::join('auteurs', 'enseignements.id_auteurs', '=', 'auteurs.id')
                            ->join('themes', 'enseignements.id_themes', '=', 'themes.id')
                            ->join('suggestions', 'enseignements.id', '=', 'suggestions.id_enseignements')
                            ->select('enseignements.id', 'enseignements.titre', 'enseignements.resume', 'enseignements.image', 'enseignements.date', 'enseignements.vues', 'auteurs.id as aut_id', 'auteurs.nom as aut_nom', 'auteurs.prenom as aut_prenom', 'auteurs.photo as aut_photo', 'auteurs.fonction as aut_fonction', 'auteurs.description as aut_description', 'themes.titre as th_titre', 'themes.description as th_description', 'themes.image as th_image')
                            ->whereIn('id_abonnements', $abonnements)
                            ->limit(4)
                            ->get();
 
        return $response->withJson($enseignements);
   }

   public function similairesAuDernier($request, $response, $args) {
       //retrieve the last listened
       $player      = Player::where('id_abonnes', '=', $_SESSION['abonnements']->id_abonnes)->first();
       $en_cours    = explode(",", $player->en_cours);
       //get the theme of the last listened
       $id_themes   = Enseignement::find($en_cours[0])->id_themes;

       //retrieve four enseignements with the same theme as the last listened
       $abonnements        = explode(",", $_SESSION['abonnements']->abonnements);

        $enseignements      = Enseignement::join('auteurs', 'enseignements.id_auteurs', '=', 'auteurs.id')
                            ->join('themes', 'enseignements.id_themes', '=', 'themes.id')
                            ->select('enseignements.id', 'enseignements.titre', 'enseignements.resume', 'enseignements.image', 'enseignements.date', 'enseignements.vues', 'auteurs.id as aut_id', 'auteurs.nom as aut_nom', 'auteurs.prenom as aut_prenom', 'auteurs.photo as aut_photo', 'auteurs.fonction as aut_fonction', 'auteurs.description as aut_description', 'themes.titre as th_titre', 'themes.description as th_description', 'themes.image as th_image')
                            ->whereIn('id_abonnements', $abonnements)
                            ->where('id_themes', '=', $id_themes)
                            ->limit(4)
                            ->get();
 
        return $response->withJson($enseignements);
   }

   public function lesPlusRegardes($request, $response, $args) {
        $abonnements        = explode(",", $_SESSION['abonnements']->abonnements);

        $enseignements      = Enseignement::join('auteurs', 'enseignements.id_auteurs', '=', 'auteurs.id')
                            ->join('themes', 'enseignements.id_themes', '=', 'themes.id')
                            ->select('enseignements.id', 'enseignements.titre', 'enseignements.resume', 'enseignements.image', 'enseignements.date', 'enseignements.vues', 'auteurs.id as aut_id', 'auteurs.nom as aut_nom', 'auteurs.prenom as aut_prenom', 'auteurs.photo as aut_photo', 'auteurs.fonction as aut_fonction', 'auteurs.description as aut_description', 'themes.titre as th_titre', 'themes.description as th_description', 'themes.image as th_image')
                            ->whereIn('id_abonnements', $abonnements)
                            ->orderBy('vues', 'DESC')
                            ->limit(5)
                            ->get();
 
        return $response->withJson($enseignements);
   }

   public function lesNouvautes($request, $response, $args) {
       $abonnements        = explode(",", $_SESSION['abonnements']->abonnements);

        $enseignements      = Enseignement::join('auteurs', 'enseignements.id_auteurs', '=', 'auteurs.id')
                            ->join('themes', 'enseignements.id_themes', '=', 'themes.id')
                            ->select('enseignements.id', 'enseignements.titre', 'enseignements.resume', 'enseignements.image', 'enseignements.date', 'enseignements.vues', 'auteurs.id as aut_id', 'auteurs.nom as aut_nom', 'auteurs.prenom as aut_prenom', 'auteurs.photo as aut_photo', 'auteurs.fonction as aut_fonction', 'auteurs.description as aut_description', 'themes.titre as th_titre', 'themes.description as th_description', 'themes.image as th_image')
                            ->whereIn('id_abonnements', $abonnements)
                            ->latest('enseignements.date')
                            ->first();

        return $response->withJson($enseignements);

   }

   public function lesPlusAimes($request, $response, $args) {
       $mergeFavoris = array();
       $countFavoris = array();

       $favoris  = Favori::all();

       //merge array
       foreach ($favoris as $favori) {
           $mergeFavoris = array_merge($mergeFavoris, explode(",", $favori->enseignements));
        }

        $countFavoris = array_count_values($mergeFavoris);

        $lesPlusAimes = array_slice(array_keys($countFavoris), 0, 4); // les huits plus liker parmis tous les abonnés

        $abonnements        = explode(",", $_SESSION['abonnements']->abonnements);

        $enseignements      = Enseignement::join('auteurs', 'enseignements.id_auteurs', '=', 'auteurs.id')
                            ->join('themes', 'enseignements.id_themes', '=', 'themes.id')
                            ->select('enseignements.id', 'enseignements.titre', 'enseignements.resume', 'enseignements.image', 'enseignements.date', 'enseignements.vues', 'auteurs.id as aut_id', 'auteurs.nom as aut_nom', 'auteurs.prenom as aut_prenom', 'auteurs.photo as aut_photo', 'auteurs.fonction as aut_fonction', 'auteurs.description as aut_description', 'themes.titre as th_titre', 'themes.description as th_description', 'themes.image as th_image')
                            ->whereIn('id_abonnements', $abonnements)
                            ->whereIn('enseignements.id', $lesPlusAimes)
                            ->get();
        
        return $response->withJson($enseignements);
   }
   
}