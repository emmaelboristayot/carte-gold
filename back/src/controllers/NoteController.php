<?php
namespace App\controllers;

use Interop\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

use App\models\Note;

class NoteController 
{
   protected $container;

   // constructor receives container instance
   public function __construct(ContainerInterface $container) {
       $this->container = $container;
   }

   public function notes($request, $response, $args) {
       $notes  = Note::where('id_abonnes', '=', $_SESSION['abonnements']->id_abonnes)->get();
        return $response->withJson($notes);
   }

   public function ajouterNote($request, $response, $args) {
       $postParam  = $request->getQueryParams();
       $note  = new Note;

        $note->id_enseignements     = $args['id_enseignements'];
        $note->id_abonnes           = $_SESSION['abonnements']->id_abonnes;
        $note->resume               = $postParam['resume'];

        $note->save();
        return $response->withJson($note);
   }

   public function modifierNote($request, $response, $args) {
        $postParam  = $request->getQueryParams();
       $note            = Note::where('id_enseignements', '=', $args['id_enseignements'])
                            ->where('id_abonnes', '=', $_SESSION['abonnements']->id_abonnes)->first();

        $note->resume   = $postParam['resume'];
        $note->save();

        return $response->withJson($note);
   }

   public function supprimerNote($request, $response, $args) {
       $id              =$args['id_enseignements'];
       $note            = Note::where('id_enseignements', '=', $args['id_enseignements'])
                            ->where('id_abonnes', '=', $_SESSION['abonnements']->id_abonnes)
                            ->delete();

        return $response->withJson([
                'deleting' => 'done',
                'description' => 'Note has been deleted',
                'code' => 200
            ]);
   }
   
}