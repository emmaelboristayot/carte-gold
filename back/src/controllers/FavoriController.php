<?php
namespace App\controllers;

use Interop\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

use App\models\Favori;

class FavoriController 
{
   protected $container;

   // constructor receives container instance
   public function __construct(ContainerInterface $container) {
       $this->container = $container;
   }

   public function favoris($request, $response, $args) {
       $favoris  = Favori::where('id_abonnes', '=', $_SESSION['abonnements']->id_abonnes)->first();

       $_SESSION['favoris']     = explode(",", $favoris->enseignements);

        return $response->withJson([
                'favoris' => $_SESSION['favoris']
            ]);
   }

   public function ajouterFavori($request, $response, $args) {
        $favoris  = Favori::where('id_abonnes', '=', $_SESSION['abonnements']->id_abonnes)->first();

        if ($favoris->count() == 1) {

            array_push($_SESSION['favoris'],$args['id_enseignements']); // add id_enseignements
            $favoris->enseignements           = implode(",", $_SESSION['favoris']);;
            $favoris->save();

        }
        else {
            $favoris    = new Favori;

            $favoris->id_abonnes    = $_SESSION['abonnements']->id_abonnes;
            $favoris->enseignements = $args['id_enseignements'];

            $favoris->save();
        }

        
        return $response->withJson([
                'adding' => 'done',
                'description' => 'Favori has been added',
                'code' => 200
            ]);
   }

   public function supprimerFavori($request, $response, $args) {
       $favoris  = Favori::where('id_abonnes', '=', $_SESSION['abonnements']->id_abonnes)->first();

        $favoris->enseignements           = implode(",", array_diff( $_SESSION['favoris'], [$args['id_enseignements']] )); // remove id_enseignements


        $favoris->save();
        return $response->withJson([
                'editing' => 'done',
                'description' => 'Favori has been removed',
                'code' => 200
            ]);
   }
   
}