<?php
namespace App\controllers;

use Interop\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

use App\models\Enseignement;
use App\models\Theme;
use App\models\Abonnement;
use App\models\Auteur;
use App\models\Adoration;
use App\models\Bible;

class EnseignementController 
{
   protected $container;

   // constructor receives container instance
   public function __construct(ContainerInterface $container) {
       $this->container = $container;
   }
   
   public function enseignements($request, $response, $args) {
       $abonnements     = explode(",", $_SESSION['abonnements']->abonnements);

        $enseignements  = Enseignement::join('auteurs', 'enseignements.id_auteurs', '=', 'auteurs.id')
                            ->join('themes', 'enseignements.id_themes', '=', 'themes.id')
                            ->select('enseignements.id', 'enseignements.titre', 'enseignements.soustitre', 'enseignements.url', 'enseignements.resume', 'enseignements.image', 'enseignements.date', 'enseignements.vues', 'auteurs.id as aut_id', 'auteurs.nom as aut_nom', 'auteurs.prenom as aut_prenom', 'auteurs.photo as aut_photo', 'auteurs.fonction as aut_fonction', 'auteurs.description as aut_description', 'themes.id as th_id', 'themes.titre as th_titre', 'themes.description as th_description', 'themes.image as th_image')
                            ->whereIn('id_abonnements', $abonnements)
                            ->orderBy('date', 'DESC')
                            ->get();
 
        return $response->withJson($enseignements);
   }

   public function countVues($request, $response, $args) {
        // your code
        $enseignement    = Enseignement::find($args['id_enseignements']);

        $enseignement->vues++;
        $enseignement->save();

        return $response->withJson($enseignement);
   }

   public function abonnements($request, $response, $args) {
       $abonnements  = Abonnement::get();
        return $response->withJson($abonnements);
   }


   public function themes($request, $response, $args) {
       $themes  = Theme::orderBy('id', 'DESC')->get();
        return $response->withJson($themes);
   }

   public function auteurs($request, $response, $args) {
       $auteurs  = Auteur::get();
        return $response->withJson($auteurs);
   }

   public function adorations($request, $response, $args) {
        $adorations  = Adoration::orderBy('id', 'DESC')->get();
        return $response->withJson($adorations);
    }

    public function bibleAudio($request, $response, $args) {
        $bibleAudio  = Bible::get();
        return $response->withJson($bibleAudio);
    }
   
}