<?php
namespace App\controllers;

use Interop\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

use App\models\Inscrit;
use App\models\Abonne;

class UserController 
{
   protected $container;

   // constructor receives container instance
   public function __construct(ContainerInterface $container) {
       $this->container = $container;
   }
   
   public function connexion($request, $response, $args) {
        // your code
        // to access items in the container... $this->container->get('');
        $postParam  = json_decode($request->getBody());
        
        $email      = $postParam->email;
        $password   = md5($postParam->password);

        $inscrit       = Inscrit::where('email', '=', $email)
                            ->where('password', '=', $password);
                            
        if ($inscrit->count() == 1) {

            $abonnements    = Abonne::where('id_inscrits', '=', $inscrit->first()->id);

            $_SESSION['user']           = $inscrit->first();
            $_SESSION['abonnements']    = $abonnements->first();

            $_SESSION['user']['abonnements'] = $_SESSION['abonnements'];

            return $response->withJson($_SESSION['user']);
        }
        else {
            return $response->withJson([
                'permission' => 'denied',
                'description' => 'user not authenticated',
                'code' => 300
                ])->withStatus(401);
        }

   }
   
   public function deconnexion($request, $response, $args) {
        //close session
        session_unset();
        session_destroy();

        return $response->withJson([
                'user' => 'logout',
                'description' => 'user is disconnected',
                'code' => 200
                ]);
   }

   public function inscription($request, $response, $args) {
        // your code
        // to access items in the container... $this->container->get('');

        $postParam  = json_decode($request->getBody());

        $inscrit    = new Inscrit;

        $inscrit->nom       = $postParam->nom;
        $inscrit->prenom    = $postParam->prenom;
        $inscrit->telephone = $postParam->telephone;
        $inscrit->email     = $postParam->email;
        $inscrit->statut    = $postParam->statut;
        $inscrit->password  = md5($postParam->password);

        $inscrit->save();

        return $response->withJson($inscrit);
   }

   public function modification($request, $response, $args) {
        // your code
        $postParam  = json_decode($request->getBody());
        $inscrit    = Inscrit::find($_SESSION['user']->id);

        $inscrit->nom       = $postParam->nom;
        $inscrit->prenom    = $postParam->prenom;
        $inscrit->telephone = $postParam->telephone;
        $inscrit->email     = $postParam->email;
        $inscrit->statut    = $postParam->statut;
        $inscrit->password  = md5($postParam->password);
        $inscrit->save();

        return $response->withJson($inscrit);
   }

   public function changePassword($request, $response, $args) {
        // your code
        $postParam  = json_decode($request->getBody());
        $inscrit    = Inscrit::find($postParam->id);

        $inscrit->password  = md5($postParam->password);
        $inscrit->save();

        return $response->withJson($inscrit);
   }

    public function quisuisje($request, $response, $args) {
        //close session
        if($_SESSION['user']){
            return $response->withJson($_SESSION['user']);
        }
        else{
            return $response->withJson([
                'permission' => 'denied',
                'description' => 'user not authenticated',
                'code' => 300
                ])->withStatus(401);
        }
   }
}