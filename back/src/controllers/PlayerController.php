<?php
namespace App\controllers;

use Interop\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

use App\models\Player;

class PlayerController 
{
   protected $container;

   // constructor receives container instance
   public function __construct(ContainerInterface $container) {
       $this->container = $container;
   }

   public function enCours($request, $response, $args) {

       $player      = Player::where('id_abonnes', '=', $_SESSION['abonnements']->id_abonnes)->first();

       $en_cours    = explode(",", $player->en_cours);

       $id_enseignements    = $en_cours[0];
       $times               = $en_cours[1];

        return $response->withJson([
                    'id_enseignements' => $id_enseignements,
                    'times' => $times
                ]);
   }

   public function saveEnCours($request, $response, $args) {

       $player  = Player::where('id_abonnes', '=', $_SESSION['abonnements']->id_abonnes);

        $postParam  = $request->getQueryParams();

        $id_enseignements       = $args['id_enseignements'];
        $times                  = $postParam['times'];

       if ($player->count() == 1) {
            $player->update(['en_cours' => $id_enseignements.','.$times]);

        }
        else {
            $player    = new player;

            $player->id_abonnes     = $_SESSION['abonnements']->id_abonnes;
            $player->en_cours       = $id_enseignements.','.$times;

            $player->save();
        }

        return $response->withJson([
                    'editing' => 'done',
                    'description' => 'Enseignement state has been added',
                    'code' => 200
                ]);
   }

   public function ecoutes($request, $response, $args) {

       $player  = Player::where('id_abonnes', '=', $_SESSION['abonnements']->id_abonnes)->first();

       $ecoutes = explode(",", $player->ecoutes);

        return $response->withJson($ecoutes);
   }

   public function addEcoutes($request, $response, $args) {

       $player  = Player::where('id_abonnes', '=', $_SESSION['abonnements']->id_abonnes)->first();

       $ecoutes = explode(",", $player->ecoutes);
       array_push($ecoutes,$args['id_enseignements'].'|'.date("Y-m-d H:i:s"));

       if ($player->count() == 1) {

            $player->ecoutes = implode(",", $ecoutes);
            $player->save();

        }
        else {
            $player    = new player;

            $player->id_abonnes     = $_SESSION['abonnements']->id_abonnes;
            $player->ecoutes        = implode(",", $ecoutes);

            $player->save();
        }

        return $response->withJson([
                    'adding' => 'done',
                    'description' => 'New enseignement has been added',
                    'code' => 200
                ]);
   }
   
}