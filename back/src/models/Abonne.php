<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Abonne extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}