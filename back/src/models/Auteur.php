<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Auteur extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
}