import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';

if (environment.production) {
  enableProdMode();
}

//platformBrowserDynamic().bootstrapModule(AppModule);

platformBrowserDynamic().bootstrapModule(AppModule)
.then(() => {
  registerServiceWorker('OneSignalSDKWorker');
})
.catch(err => console.log(err));

function registerServiceWorker(swName: string) {
  if (environment.production) {
    if ('serviceWorker' in navigator) {
      navigator.serviceWorker
        .register(`/${swName}.js`)
        .then(() => {
          console.log('Installation successful!');
        }, (err) => {
          console.log('Service Worker registration failed: ', err);
        });
    } else {
      console.error('[App] Service Worker API is not supported in current browser');
    }
  }
}