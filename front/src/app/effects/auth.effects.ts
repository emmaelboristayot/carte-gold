import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Action, Store } from '@ngrx/store';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { of } from 'rxjs/observable/of';
import { tap, map, exhaustMap, catchError, mergeMap } from 'rxjs/operators';

import { UserService } from '../services/user.service';

import * as userReducer from './../reducers/user/user.reducer';

import * as authAction from './../reducers/auth/auth.actions';
import * as userAction from './../reducers/user/user.actions';

import { User, Authenticate } from './../models/user.model';

import { MatSnackBar } from '@angular/material';

@Injectable()
export class AuthEffects {

    @Effect()
    login$ = this.actions$.pipe(
        ofType(authAction.LOGIN),
        mergeMap((action: authAction.Login) =>
            this.userService
                .connexion(action.payload).pipe(
                    map((user: User) => new authAction.LoginSuccess(user)),
                    catchError(error => of(new authAction.LoginFailure(error)))
                )
        ));

    @Effect()
    logout$ = this.actions$.pipe(
        ofType(authAction.LOGOUT),
        mergeMap((action: authAction.Logout) =>
            this.userService
                .deconnexion().pipe(
                    map((user: User) => {
                        new userAction.DeleteUser()
                        return new authAction.LoginRedirect()
                    }),
                    catchError(error => of(new authAction.LoginRedirect()))
                )
        ));

    @Effect()
    userSession$ = this.actions$.pipe(
        ofType(authAction.USERSESSION),
        mergeMap((action: authAction.UserSession) =>
            this.userService
                .quisuisje().pipe(
                    map((user: User) => new authAction.LoginSuccess(user)),
                    catchError(error => of(new authAction.LoginFailure(error)))
                )

        ));

    @Effect({ dispatch: false })
    loginSuccess$ = this.actions$.pipe(
        ofType(authAction.LOGINSUCCESS),
        tap((action: authAction.LoginSuccess) => {
            this._store.dispatch(new userAction.SetUser(action.payload))
            this.router.navigate(['/enseignements'])
        }));

    @Effect({ dispatch: false })
    loginRedirect$ = this.actions$.pipe(
        ofType(authAction.LOGINREDIRECT, authAction.LOGOUT),
        tap(() => this.router.navigate(['']))
    );

    @Effect({ dispatch: false })
    loginFailure$ = this.actions$.pipe(
        ofType(authAction.LOGINFAILURE),
        tap(() => this.showErrorMessage())
    );

    constructor(
        private actions$: Actions,
        private userService: UserService,
        private router: Router,
        private _store: Store<userReducer.State>,
        public snackBar: MatSnackBar
    ) { }

    showErrorMessage() {
        this.snackBar.open("Identifiant ou mot de passe incorrect !", "", {
            duration: 3000,
        });
    }
}