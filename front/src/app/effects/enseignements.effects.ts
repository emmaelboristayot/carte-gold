import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Action, Store } from '@ngrx/store';
import { Effect, Actions, ofType } from '@ngrx/effects';
import { of } from 'rxjs/observable/of';
import { tap, map, exhaustMap, catchError, mergeMap } from 'rxjs/operators';
//Services
import { EnseignementService } from '../services/enseignement.service';

//model
import { Enseignement } from '../models/enseignement.model';
import { Theme } from '../models/theme.model';

//Reducers
import * as enseignementsReducer from './../reducers/enseignements/enseignements.reducer';
import * as themesReducer from './../reducers/themes/themes.reducer';

//Actions
import * as enseignementsActions from './../reducers/enseignements/enseignements.actions';
import * as themesActions from './../reducers/themes/themes.actions';
import * as authAction from './../reducers/auth/auth.actions';


@Injectable()
export class EnseignementsEffects {

    @Effect()
    loadEnseignements$ = this.actions$.pipe(
        ofType(enseignementsActions.LOAD_ENSEIGNEMENTS),
        mergeMap((action: enseignementsActions.LoadEnseignements) =>
            this.enseignementService
                .getEnseignements().pipe(
                    map((enseignements: Enseignement[]) => new enseignementsActions.SetEnseignements(enseignements)),
                    catchError(error => of(new authAction.LoginRedirect()))
                )
        ));

    @Effect()
    loadThemes$ = this.actions$.pipe(
        ofType(themesActions.LOAD_THEMES),
        mergeMap((action: themesActions.LoadThemes) =>
            this.enseignementService
                .getThemes().pipe(
                    map((themes: Theme[]) =>  new themesActions.SetThemes(themes)),
                    catchError(error => of(new authAction.LoginRedirect()))
                )
        ));

    constructor(
        private actions$: Actions,
        private enseignementService: EnseignementService
    ) { }

}