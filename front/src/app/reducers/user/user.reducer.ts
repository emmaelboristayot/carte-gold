import { ActionReducer} from '@ngrx/store';
import * as UserActions from './user.actions';
import { User } from './../../models/user.model';

export type Action = UserActions.All;
export interface State {
    user : User;
}

const loadState = () => {
    try {
        const serializedState = localStorage.getItem('user');
        if (serializedState === null) {
            return
        }
        return JSON.parse(serializedState);
    } catch (err) {
        return
    }
}
const saveState = (state) => {
    try {
        const serializedState = JSON.stringify(state);
        localStorage.setItem('user', serializedState);
        return state;
    } catch (err) {
        // Ignore err
    }
}
const deleteState = () => {
    try {
        localStorage.removeItem('user');
    } catch (err) {
        return
    }
}

export function reducer(state = loadState(), action: Action) {
    switch (action.type) {
        case UserActions.SET_USER:
            state = saveState(action.payload);
            return state;

        case UserActions.GET_USER:
            return state;

        case UserActions.DELETE_USER:
            deleteState();
            return ;

        default:
            return state;
    }
}


export const getUser = (state: State) => state.user;