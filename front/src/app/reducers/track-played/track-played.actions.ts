import { Action } from '@ngrx/store';
import { TrackPlayed } from './../../models/track-played.model';

export const SET_TRACK_PLAYED = '[TrackPlayed] set';
export const GET_TRACK_PLAYED = '[TrackPlayed] get';
export const PLAY_TRACK_PLAYED = '[TrackPlayed] play';
export const PAUSE_TRACK_PLAYED = '[TrackPlayed] pause';
export const PLAY_OR_PAUSE_TRACK_PLAYED = '[TrackPlayed] play-or-pause';

export class SetTrackPlayed implements Action {
    readonly type = SET_TRACK_PLAYED;
    constructor(public payload: TrackPlayed) {}
}

export class GetTrackPlayed implements Action {
    readonly type = GET_TRACK_PLAYED;
}

export class PlayTrackPlayed implements Action {
    readonly type = PLAY_TRACK_PLAYED;
    constructor(public payload: TrackPlayed) {}
}

export class PauseTrackPlayed implements Action {
    readonly type = PAUSE_TRACK_PLAYED;
    constructor(public payload: TrackPlayed) {}
}

export class PlayOrPauseTrackPlayed implements Action {
    readonly type = PLAY_OR_PAUSE_TRACK_PLAYED;
    constructor(public payload: TrackPlayed) {}
}

export type All
    = SetTrackPlayed
    | GetTrackPlayed
    | PlayTrackPlayed
    | PauseTrackPlayed
    | PlayOrPauseTrackPlayed;