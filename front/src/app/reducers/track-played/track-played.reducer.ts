import { EnseignementConstant } from './../../constants/enseignement.constant';
import { TrackPlayed } from './../../models/track-played.model';
import { ActionReducer } from '@ngrx/store';
import { isNullOrUndefined } from "util";
import * as TrackPlayedActions from './track-played.actions';

export type Action = TrackPlayedActions.All;

export interface State {
    trackPlayed : TrackPlayed;
}

const loadState = () => {
    try {
        const serializedState = localStorage.getItem('track_played')
        if (serializedState === null) {
            return
        }
        return JSON.parse(serializedState)
    } catch (err) {
        return
    }
}
const saveState = (state) => {
    try {
        const serializedState = JSON.stringify(state)
        localStorage.setItem('track_played', serializedState)
        return state
    } catch (err) {
        // Ignore err
    }
}

export function reducer(state : TrackPlayed , action: Action) {
    switch (action.type) {
        case TrackPlayedActions.SET_TRACK_PLAYED:
            state = action.payload
            return state;

        case TrackPlayedActions.GET_TRACK_PLAYED:
            return state;

        case TrackPlayedActions.PLAY_TRACK_PLAYED:
            if (state.player !== action.payload.player && !isNullOrUndefined(state.player)) {
                state.player.pause();
            }
            state.player = action.payload.player;

            if (state.player.paused) {
                state.player.play();
            }
            return state;

        case TrackPlayedActions.PAUSE_TRACK_PLAYED:

            if (state.player !== action.payload.player && !isNullOrUndefined(state.player)) {
                state.player.pause();
            }
            state = action.payload;
            if (state.player.played) {
                state.player.pause();
            }
            return state;

        case TrackPlayedActions.PLAY_OR_PAUSE_TRACK_PLAYED:
            state = (state) ? state : {track: EnseignementConstant.DEFAULT, player: null}

            if (state.player !== action.payload.player && !isNullOrUndefined(state.player)) {
                state.player.pause();
            }
            state = action.payload;

            if (state.player.paused) {
                state.player.play();
            }
            else {
                state.player.pause();
            }
            return state;

        default:
            return state;
    }
}

export const getTrackPlayed = (state: State) => state.trackPlayed;
