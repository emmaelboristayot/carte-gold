import { ActionReducer } from '@ngrx/store';
import * as SearchActions from './search.actions';

export type Action = SearchActions.All;

export interface State {
      search : string;
  }
  

export function reducer(state, action: Action) : State{
   switch (action.type) {
      case SearchActions.SET_SEARCH:
         state = action.payload;
         return state;

      case SearchActions.GET_SEARCH: 
         return state;

      default:
         return state;
   }
}

export const getSearch = (state: State) => state.search;