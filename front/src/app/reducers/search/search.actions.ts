import { Action } from '@ngrx/store';

export const SET_SEARCH  = '[Search] set';
export const GET_SEARCH  = '[Search] get';

export class SetSearch implements Action {
  readonly type = SET_SEARCH;
  constructor(public payload: string) {}
}

export class GetSearch implements Action {
  readonly type = GET_SEARCH;
}

export type All
  = SetSearch
  | GetSearch;