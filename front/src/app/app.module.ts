//Service
import { EnseignementService } from './services/enseignement.service';
import { CommonService } from './shared/common.service';
import { UserService } from './services/user.service';

//Module
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import 'hammerjs';
import { AudioPlayerModule } from './components/audio-player/audio-player.module';
import { buttonModule } from './shared/button/button.module';
import { MatSidenavModule, MatTabsModule, MatCardModule, MatIconModule, MatSnackBarModule, MatButtonModule } from '@angular/material';


//Component
import { AppComponent } from './app.component';
import { LoginPageComponent } from './components/login-page/login-page.component';
import { ModalInscriptionComponent } from './components/modal-inscription/modal-inscription.component';
import { TeachingPageComponent } from './components/teaching-page/teaching-page.component';
import { HeaderLoginComponent } from './components/headers/header-login/header-login.component';
import { HeaderTeachingComponent } from './components/headers/header-teaching/header-teaching.component';
import { SearchBarComponent } from './shared/search-bar/search-bar.component';
import { ThemeCardComponent } from './components/teaching-page/theme-card/theme-card.component';
import { FormLoginComponent } from './components/login-page/form-login/form-login.component';
import { ChangePasswordComponent } from './components/change-password/change-password.component';
import { FooterComponent } from './components/footer/footer.component';
import { ThemeTeachingComponent } from './components/theme-teaching/theme-teaching.component';
import { SidenavComponent } from './components/headers/sidenav/sidenav.component';

//Forms
import { ReactiveFormsModule } from '@angular/forms';
import { FormsModule }  from '@angular/forms';

//Routers
import { AppRoutingModule }        from './app-routing.module';

//Reducer
import { StoreModule } from '@ngrx/store';
import { reducers } from './reducers';

//TERADATA COVALENT
import { CovalentNotificationsModule } from '@covalent/core';
import { CovalentSearchModule } from '@covalent/core';

//Filter
import {NgPipesModule} from 'ngx-pipes';

//Effects
import { EffectsModule } from '@ngrx/effects';
import { AuthEffects } from './effects/auth.effects';
import { EnseignementsEffects } from './effects/enseignements.effects';

//Jquery
import * as $ from 'jquery';


@NgModule({
  declarations: [
    AppComponent,
    LoginPageComponent,
    ModalInscriptionComponent,
    TeachingPageComponent,
    HeaderLoginComponent,
    HeaderTeachingComponent,
    SearchBarComponent,
    ThemeCardComponent,
    ThemeTeachingComponent,
    FormLoginComponent,
    ChangePasswordComponent,
    FooterComponent,
    SidenavComponent

  ],
  imports: [
    MatSidenavModule,
    MatCardModule,
    MatIconModule,
    MatTabsModule,
    MatSnackBarModule,
    MatButtonModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpModule,
    ReactiveFormsModule,
    AppRoutingModule,
    AudioPlayerModule,
    buttonModule,
    CovalentSearchModule,
    CovalentNotificationsModule,
    NgPipesModule,
    EffectsModule.forRoot([AuthEffects, EnseignementsEffects]),
    StoreModule.forRoot(reducers)
  ],
  providers: [EnseignementService, CommonService, UserService],
  bootstrap: [AppComponent]
})
export class AppModule { }
