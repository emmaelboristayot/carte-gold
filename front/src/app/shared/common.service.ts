export class CommonService {

  public formatTime(time) {
    let hours: any = Math.floor(time / 3600);
    let mins: any = Math.floor((time % 3600) / 60);
    let secs: any = Math.floor(time % 60);

    if (secs < 10) {
      secs = "0" + secs;
    }

    if (hours) {
      if (mins < 10) {
        mins = "0" + mins;
      }

      return hours + ":" + mins + ":" + secs; // hh:mm:ss
    } else {
      return mins + ":" + secs; // mm:ss
    }
  }

}
