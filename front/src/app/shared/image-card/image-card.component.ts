import { Component,Input, HostListener } from '@angular/core';
import { Theme } from './../../models/theme.model';
import { environment } from '../../../environments/environment';
import { defaultImage } from './default-image.base64';
import { Observable } from 'rxjs/Observable';
import { fromEvent } from 'rxjs/observable/fromEvent';

@Component({
  selector: 'image-card',
  templateUrl: './image-card.component.html',
  styleUrls: ['./image-card.component.scss']
})
export class ImageCardComponent{

  @Input() theme: Theme;
  public env : any;
  public defaultImage : string;

  public scrollTarget$ : Observable<any>;

  constructor() {
    this.env = environment;
    this.defaultImage = defaultImage;
  }

}
