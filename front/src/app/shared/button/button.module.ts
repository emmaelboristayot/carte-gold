import { NgModule }       from '@angular/core';
import { CommonModule }   from '@angular/common';
import { FormsModule }    from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { MatButtonModule, MatCheckboxModule, MatIconModule } from '@angular/material';
import { DropdownMenuComponent } from './dropdown-menu/dropdown-menu.component';
import { DropdownUserComponent } from './dropdown-user/dropdown-user.component';
import { NavNotificationComponent } from './nav-notification/nav-notification.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCheckboxModule,
    MatIconModule
  ],
  exports: [
    DropdownMenuComponent,
    DropdownUserComponent,
    NavNotificationComponent
  ],
  declarations: [
    DropdownMenuComponent,
    DropdownUserComponent,
    NavNotificationComponent
  ],
  providers: [ ]
})
export class buttonModule {}