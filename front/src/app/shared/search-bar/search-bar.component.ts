import { Component, OnInit} from '@angular/core';
import { State, Store } from '@ngrx/store'
//reducer
import * as searchReducer from './../../reducers/search/search.reducer';
import * as searchActions from './../../reducers/search/search.actions';

@Component({
  selector: 'search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss']
})
export class SearchBarComponent implements OnInit {

  public searchInputTerm : string;

  constructor(
    private _store: Store<searchReducer.State>) { 
      this._store.select(searchReducer.getSearch);
    }

  ngOnInit() {
  }

  public setSearchValue($event) {
     this._store.dispatch(new searchActions.SetSearch($event));
  }

}
