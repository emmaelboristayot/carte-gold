import { Enseignement } from './enseignement.model';

export class TrackPlayed{

  constructor(
    public track: Enseignement,
    public player:HTMLAudioElement
    ) { }

}
