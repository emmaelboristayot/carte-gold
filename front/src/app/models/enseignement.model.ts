export class Enseignement{

  constructor(
    public id: number,
    public titre:string,
    public soustitre:string,
    public url:string,
    public resume:string,
    public image:string,
    public date:string,
    public vues: number,
    public aut_id: number,
    public aut_nom:string,
    public aut_prenom:string,
    public aut_photo:string,
    public aut_fonction:string,
    public aut_description:string,
    public th_id:number,
    public th_titre:string,
    public th_description:string,
    public th_image:string,
    ) { }

}
