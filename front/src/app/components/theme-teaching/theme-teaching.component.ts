import { Component, OnInit } from '@angular/core';
import { EnseignementService } from '../../services/enseignement.service';
import { Store } from '@ngrx/store'
import {Router, ActivatedRoute, Params} from '@angular/router';
//model
import { Enseignement } from '../../models/enseignement.model';
import { Theme } from '../../models/theme.model';
import { environment } from '../../../environments/environment';
//pipes
import {FilterByPipe} from "ngx-pipes";

//Reducers
import * as enseignementsReducer from './../../reducers/enseignements/enseignements.reducer';
import * as searchReducer from './../../reducers/search/search.reducer';
import * as themesReducer from './../../reducers/themes/themes.reducer';

//Actions
import * as searchActions from './../../reducers/search/search.actions';

@Component({
  selector: 'theme-teaching',
  templateUrl: './theme-teaching.component.html',
  styleUrls: ['./theme-teaching.component.scss'],
  providers: [FilterByPipe]
})
export class ThemeTeachingComponent implements OnInit {
  public  _enseignements: Array<Enseignement>;
  public _themes: Array<Theme>;
  public _theme: Theme;
  public searchValue: string;
  public env: any;

  constructor(
    private filterByPipe: FilterByPipe,
    private _enseignementService: EnseignementService,
    private _store_search: Store<searchReducer.State>,
    private _store_enseignements: Store<enseignementsReducer.State>,
    private _store_themes: Store<themesReducer.State>,
    private route: ActivatedRoute,
    private router: Router) { 
      this.env = environment;

      this._store_enseignements.select(enseignementsReducer.getEnseignements).subscribe((data : Enseignement[]) => {
          this._enseignements = data;
      });

      this._store_search.select(searchReducer.getSearch).subscribe((data : string) => {
          this.searchValue = data;
      });

      this._store_themes.select(themesReducer.getThemes).subscribe((data : Array<Theme>) => {
          this._themes = data;
      });
    }

  ngOnInit() {
    // subscribe to router event
    this.route.params.subscribe((params: Params) => {
        let id = params['id'];
        this.setEnseignements(id);
        this.setTheme(id);
      });

      this._store_search.dispatch(new searchActions.SetSearch(""));
  }


  public setEnseignements(id : number) {
    let enseignements = this.filterByPipe.transform(
      this._enseignements,
      ['th_id'],
      id,
      true
    );
    this._enseignements = enseignements;
  }

  setTheme(id : number) {
    let theme = this.filterByPipe.transform(
      this._themes,
      ['id'],
      id,
      true
    );
    this._theme = theme[0];
  }

  goBack() {
    this.router.navigate([`/enseignements`]);
  }

}
