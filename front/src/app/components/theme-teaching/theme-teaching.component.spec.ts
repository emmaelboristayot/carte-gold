import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThemeTeachingComponent } from './theme-teaching.component';

describe('ThemeTeachingComponent', () => {
  let component: ThemeTeachingComponent;
  let fixture: ComponentFixture<ThemeTeachingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThemeTeachingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThemeTeachingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
