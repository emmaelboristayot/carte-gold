import { Component, OnInit, Input } from '@angular/core';
import { Store } from '@ngrx/store'

//reducer
import * as userReducer from './../../../reducers/user/user.reducer';
import * as userAction from './../../../reducers/user/user.actions';

import * as authReducer from './../../../reducers/auth/auth.reducer';
import * as authAction from './../../../reducers/auth/auth.actions';

@Component({
  selector: 'sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss']
})
export class SidenavComponent implements OnInit {
  @Input() sidenav: any;
  user: any;

  constructor(
    private _store: Store<userReducer.State>,
    private _store_auth: Store<authReducer.State>) {
    this._store.select(userReducer.getUser).subscribe((data) => {
      this.user = data;
    })
    this._store_auth.select(authReducer.getUser)
  }

  ngOnInit() {

    this.getUser();
  }

  getUser() {
    this._store.dispatch(new userAction.GetUser())
  }

  deleteUser() {
    this._store.dispatch(new userAction.DeleteUser())
  }

  logout() {
    this._store_auth.dispatch(new authAction.Logout())
  }

}
