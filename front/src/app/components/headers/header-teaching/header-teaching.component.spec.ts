import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderTeachingComponent } from './header-teaching.component';

describe('HeaderTeachingComponent', () => {
  let component: HeaderTeachingComponent;
  let fixture: ComponentFixture<HeaderTeachingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderTeachingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderTeachingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
