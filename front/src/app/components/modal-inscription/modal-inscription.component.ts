import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'modal-inscription',
  templateUrl: './modal-inscription.component.html',
  styleUrls: ['./modal-inscription.component.scss']
})
export class ModalInscriptionComponent implements OnInit {
  user: FormGroup;
  modal : any;

  validationMessages = {
    'nom': {
      'required': 'Ce champ est obligatoire.'
    },
    'prenom': {
      'required': 'Ce champ est obligatoire.'
    },
    'telephone': {
      'required': 'Ce champ est obligatoire.'
    },
    'email': {
      'required': 'Ce champ est obligatoire.'
    },
    'statut': {
      'required': 'Ce champ est obligatoire.'
    },
    'password': {
      'required': 'Ce champ est obligatoire.'
    }
  };

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    this.modal = $('#inscription');

    this.user = this.fb.group({
      nom: ['', [Validators.required]],
      prenom: ['', [Validators.required]],
      telephone: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      statut: ['', [Validators.required]],
      password: ['', [Validators.required]]
      
    });
  }

  show() {
    this.modal.modal('show');
  }

  onSubmit() {
  }

}
