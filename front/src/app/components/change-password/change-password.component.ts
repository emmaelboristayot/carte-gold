import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store'
import { UserService } from '../../services/user.service';
import { environment } from '../../../environments/environment';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { passwordMatcher } from '../../services/password-matcher.service';

//Reducers
import * as userReducer from './../../reducers/user/user.reducer';
import * as userActions from './../../reducers/user/user.actions';

@Component({
  selector: 'change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {
  public _user: any;
  public modal: any;
  public errorMessage: string;
  user: FormGroup;

  constructor(
    private _store: Store<userReducer.State>,
    private _userService: UserService,
    private fb: FormBuilder) {
    this._store.select(userReducer.getUser).subscribe((data) => {
      this._user = data;
    });
  }

  ngOnInit() {
    this.modal = $('#passwordModal');
    this.modaleShow(this._user);

    this.user = this.fb.group({
      password: ['', [Validators.required, Validators.minLength(8)]],
      confirm: ['', [Validators.required, Validators.minLength(8), passwordMatcher]]
    });
  }

  modaleShow(user) {
    if (user.password === environment.default_password) {
      this.modal.modal({
        keyboard: false,
        backdrop: true,
        show: true
      });
    }
  }

  onSubmit() {
    let data = { 
      id : this._user.id,
      password : this.user.value.password
    };
    this._userService
      .changePassword(data)
      .subscribe(
      value => {

        this._store.dispatch(new userActions.SetUser(value));

        this.modal.modal('hide');

      },
      error => {
        this.errorMessage = <any>error;
      });

  }

  onChange($event) {
    this.user.get('confirm').setValue('');
  }

  passwordFormError(field) {
    if ((( this.user.get(field).hasError('required') || this.user.get(field).hasError('minlength')) && this.user.get(field).touched )) {
      return true;
    }
    if ((( !this.user.get(field).hasError('required') || !this.user.get(field).hasError('minlength')) && this.user.get(field).touched)) {
      return false;
    }
  }

  confirmFormError(field) {
    if ((( this.user.get(field).hasError('required') || this.user.get(field).hasError('minlength') || this.user.get(field).hasError('passwordMatcher')) && this.user.get(field).touched )) {
      return true;
    }
    if ((( !this.user.get(field).hasError('required') || !this.user.get(field).hasError('minlength') || !this.user.get(field).hasError('passwordMatcher')) && this.user.get(field).touched)) {
      return false
    }
  }

}
