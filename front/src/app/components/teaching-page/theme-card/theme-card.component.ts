import { Router } from '@angular/router';
import { Component, OnInit, Input } from '@angular/core';
import { Theme } from './../../../models/theme.model';
import {FilterByPipe} from "ngx-pipes";
import { Store } from '@ngrx/store'
import { Enseignement } from '../../../models/enseignement.model';
import { environment } from '../../../../environments/environment';

//Reducers
import * as enseignementsReducer from './../../../reducers/enseignements/enseignements.reducer';

@Component({
  selector: 'theme-card',
  templateUrl: './theme-card.component.html',
  styleUrls: ['./theme-card.component.scss']
})
export class ThemeCardComponent implements OnInit {

  @Input() theme: Theme;
  private _enseignements: Array<Enseignement>;
  public env : any;

  constructor(
    private _store: Store<enseignementsReducer.State>,
    private filterByPipe: FilterByPipe,
    private router: Router
  ) {
    this.env = environment;
    this._store.select(enseignementsReducer.getEnseignements).subscribe((data: Enseignement[]) => {
      this._enseignements = data;
    });

  }

  ngOnInit() {
  }

  public getEnseignements(): Array<Enseignement> {
    let enseignements = this.filterByPipe.transform(
      this._enseignements,
      ['th_id'],
      this.theme.id,
      true
    );
    return enseignements;
  }

  public countEnseignement(): number {

    return this.getEnseignements().length;
  }

  public goToTeaching() {

    this.router.navigate([`/theme-enseignement/${this.theme.id}/`]);
  }

}
