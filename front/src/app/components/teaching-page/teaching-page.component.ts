import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store'
//model
import { Enseignement } from '../../models/enseignement.model';
import { Theme } from '../../models/theme.model';
//constant
import { EnseignementConstant } from './../../constants/enseignement.constant';
//pipes
import {FilterByPipe} from "ngx-pipes";

//Reducers
import * as enseignementsReducer from './../../reducers/enseignements/enseignements.reducer';
import * as searchReducer from './../../reducers/search/search.reducer';
import * as themesReducer from './../../reducers/themes/themes.reducer';

//Actions
import * as enseignementsActions from './../../reducers/enseignements/enseignements.actions';
import * as searchActions from './../../reducers/search/search.actions';
import * as themesActions from './../../reducers/themes/themes.actions';


@Component({
  selector: 'app-teaching-page',
  templateUrl: './teaching-page.component.html',
  styleUrls: ['./teaching-page.component.scss'],
  providers: [FilterByPipe]
})
export class TeachingPageComponent implements OnInit {
  public errorMessage: string;
  public  _enseignements: Array<Enseignement> = [];
  public  _lastEnseignement: Enseignement = EnseignementConstant.DEFAULT;
  public _themes: Array<Theme> = [];
  public searchValue: string;

  constructor(
    private filterByPipe: FilterByPipe,
    private _store_search: Store<searchReducer.State>,
    private _store_enseignements: Store<enseignementsReducer.State>,
    private _store_themes: Store<themesReducer.State>) { 

      this._store_search.select(searchReducer.getSearch).subscribe((data : string) => {
          this.searchValue = data;
      });

      this._store_enseignements.select(enseignementsReducer.getEnseignements).subscribe((data ) => {
          this._enseignements = data;
          if (data) {
            this._lastEnseignement = data[0];
          }
      });

      this._store_themes.select(themesReducer.getThemes).subscribe((data ) => {
        this._themes = data;
      });

    }

  ngOnInit() {
    this.getEnseignements();
    this.getThemes();
  }


  getEnseignements() {
    this._store_enseignements.dispatch(new enseignementsActions.LoadEnseignements());
  }

  getThemes() {
    this._store_themes.dispatch(new themesActions.LoadThemes());
  }

  getLastEnseignement(): Enseignement{
    return this._lastEnseignement;
  }

}
