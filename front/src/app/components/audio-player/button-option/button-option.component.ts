import {Component, Input, OnInit} from '@angular/core';
//service
import { EnseignementService } from '../../../services/enseignement.service';
import {Enseignement} from '../../../models/enseignement.model';
import { environment } from '../../../../environments/environment';
//FileSaver
import * as FileSaver from 'file-saver';

@Component({
  selector: 'button-option',
  templateUrl: './button-option.html',
  styleUrls: ['./button-option.style.scss']
})


export class ButtonOptionComponent  implements OnInit {
@Input() player: HTMLAudioElement;
@Input() track: Enseignement;
@Input() sizeClass: string;
@Input() iconStyle: string;

public urlSong : string;

public iconClass : string;

  constructor(private _enseignementService: EnseignementService) {
    this.iconClass = "fa fa-thumbs-o-up";
  }

  ngOnInit() {
      let year = new Date(this.track.date).getFullYear();
      this.urlSong = `${environment.audio+year}/${this.track.url}`; //song is to folder .audio/201x/...
  }

  public repeat() {
        if (this.player.loop) {
            this.player.loop = false;
        }
        else {
            this.player.loop = true;
        }
    }

  public download() {
      this._enseignementService
          .downloadEnseignementByUrl(this.urlSong)
          .subscribe(
              value => {
                  FileSaver.saveAs(value, this.track.url);
              },
              error => {
              });
    }

}
