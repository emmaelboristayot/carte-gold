import { Component, Input } from '@angular/core';

@Component({
    selector: 'progress-bar',
    templateUrl: './progress-bar.html',
    styleUrls: ['./progress-bar.style.scss'],
})
export class ProgressBarComponent {
    @Input() player: HTMLAudioElement;
    @Input() height: string;

    public duration: number = 0;    // Durée totale
    public time: number = 0;    // Temps écoulé
    public fraction: number = 0;
    public percent: number = 0;

    public progressClass: string = "progress-bar progress-bar-striped progress-bar-animated";

    constructor() {
    }

    ngOnInit() {
        this.player.onplay = () => {
            this.progressClass = "progress-bar progress-bar-striped progress-bar-animated";
        };

        this.player.onpause = () => {
            this.progressClass = "progress-bar progress-bar-striped";
        };

        this.player.ontimeupdate = () => {
            this.update();
        };
    }

    update() {
        this.duration = this.player.duration;    // Durée totale
        this.time = this.player.currentTime; // Temps écoulé
        this.fraction = this.time / this.duration;
        this.percent = Math.ceil(this.fraction * 100);
    }

    clickProgress(control, event) {

        let parent = this.getPosition(control);    // La position absolue de la progressBar
        let target = this.getMousePosition(event); // L'endroit de la progressBar où on a cliqué

        let x = target.x - parent.x;

        let wrapperWidth = document.getElementById('progressBarControl').offsetWidth;

        let percent = Math.ceil((x / wrapperWidth) * 100);
        let duration = this.player.duration;

        this.player.currentTime = (duration * percent) / 100;
    }

    getPosition(element) {
        let top: number = 0;
        let left: number = 0;

        do {
            top += element.offsetTop;
            left += element.offsetLeft;
        } while (element = element.offsetParent);

        return { x: left, y: top };
    }

    getMousePosition(event) {
        return {
            x: event.pageX,
            y: event.pageY
        };
    }

}