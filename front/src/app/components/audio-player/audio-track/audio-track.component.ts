import { TrackPlayed } from './../../../models/track-played.model';
import { Component, OnInit, Input } from '@angular/core';
//model
import { Enseignement } from '../../../models/enseignement.model';
import { environment } from '../../../../environments/environment';
import { Theme } from './../../../models/theme.model';
//service
import { EnseignementService } from '../../../services/enseignement.service';
import { CommonService } from './../../../shared/common.service';
import { State, Store } from '@ngrx/store'
//reducer
import * as trackPlayedReducer from './../../../reducers/track-played/track-played.reducer';
import * as trackPlayedAction from './../../../reducers/track-played/track-played.actions';

@Component({
    selector: 'audio-track',
    templateUrl: './audio-track.component.html',
    styleUrls: ['./audio-track.component.scss']
})
export class AudioTrackComponent implements OnInit {
    @Input() track: Enseignement;

    public player: HTMLAudioElement;
    public trackPlayed: TrackPlayed;
    public theme: Theme;

    public duration: number = 0;    // Durée totale
    public time: number = 0;    // Temps écoulé
    public fraction: number = 0;
    public percent: number = 0;
    public env: any;
    public onPlayed: boolean = false;
    public onProgress: boolean = false;

    public urlSong: string;

    public modal: any;

    constructor(private _enseignementService: EnseignementService, 
        private _store_track_played: Store<trackPlayedReducer.State>,
        public commonService: CommonService) {
        this.env = environment;
        this._store_track_played.select(trackPlayedReducer.getTrackPlayed);

    }

    ngOnInit() {
        this.theme = new Theme(this.track.th_id, 
            this.track.th_titre, 
            this.track.th_description, 
            this.track.th_image);

        let year = new Date(this.track.date).getFullYear();
        this.urlSong = `${this.env.audio + year}/${this.track.url}`; //song is to folder .audio/201x/...
    }

    _onplaying() {
        this.onProgress = false;
    }

    _onprogress() {
    }

    _oncanplaythrough() {
    }

    _onloadeddata() {
        this.countVues();
        this.onProgress = true;
    }

    _oncanplay() {
    }

    _onplay() {
        this.onPlayed = true;
    }

    _onpause() {
        this.onProgress = false;
        this.onPlayed = false;
    }

    _onended() {
    }

    _ontimeupdate(player) {
        this.duration = player.duration;    // Durée totale
        this.time = player.currentTime; // Temps écoulé
        this.fraction = this.time / this.duration;
        this.percent = Math.ceil(this.fraction * 100);
    }

    public countVues() {
        this._enseignementService
            .countVues(this.track.id)
            .subscribe(
            value => {
                this.track.vues = value.vues;
            },
            error => {
            });
    }

    playAndPause(player: any) {
        
        this.trackPlayed = new TrackPlayed(this.track, player);
        
        this._store_track_played.dispatch(new trackPlayedAction.PlayOrPauseTrackPlayed(this.trackPlayed));
        this.openAudioDisplay();
    }

    public openAudioDisplay() {
        this.modal = $('#audioDisplayModal');
        this.modal.modal({
          keyboard: false,
          backdrop: true,
          show: true
        });
      }

}