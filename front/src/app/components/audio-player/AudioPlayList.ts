import { Enseignement } from './../../models/enseignement.model';
import { TrackPlayed } from './../../models/track-played.model';

class AudioPlayList {
    tracks: Enseignement[];
    trackPlayed: TrackPlayed;

    constructor() {
    }

    initPlayList(tracks: Enseignement[], trackPlayed: TrackPlayed) {
        this.tracks = tracks;
        this.trackPlayed = trackPlayed;
    }

    public next() {
        
    }

    public preview() {
        
    }

}