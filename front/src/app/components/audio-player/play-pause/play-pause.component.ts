import { Enseignement } from './../../../models/enseignement.model';
import { Component, Input } from '@angular/core';
import { State, Store } from '@ngrx/store'

//reducer
import * as trackPlayedReducer from './../../../reducers/track-played/track-played.reducer';
import * as trackPlayedAction from './../../../reducers/track-played/track-played.actions';

import { TrackPlayed } from '../../../models/track-played.model';

@Component({
    selector: 'play-pause',
    templateUrl: './play-pause.html',
    styleUrls: ['./play-pause.style.scss']
})


export class PlayPauseComponent {
    @Input() player: HTMLAudioElement;
    @Input() sizeClass: string;
    @Input() onPlayed: boolean;
    @Input() track: Enseignement;

    constructor(private _store: Store<trackPlayedReducer.State>) {
        this._store.select(trackPlayedReducer.getTrackPlayed).subscribe((data: TrackPlayed) => {
        })
    }

    ngOnInit() {
    }

    play() {
        let trackPlayed = new TrackPlayed(this.track, this.player);
        this._store.dispatch(new trackPlayedAction.PlayTrackPlayed(trackPlayed));
    }

    pause() {
        let trackPlayed = new TrackPlayed(this.track, this.player);
        this._store.dispatch(new trackPlayedAction.PauseTrackPlayed(trackPlayed));
    }

    playAndPause() {
        let trackPlayed = new TrackPlayed(this.track, this.player);
        this._store.dispatch(new trackPlayedAction.PlayOrPauseTrackPlayed(trackPlayed));
    }

}
