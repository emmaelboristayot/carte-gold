import { Component, OnInit } from '@angular/core';
import { TrackPlayed } from './../../../models/track-played.model';
import { CommonService } from './../../../shared/common.service';
import { EnseignementConstant } from './../../../constants/enseignement.constant';
import { environment } from '../../../../environments/environment';
//model
import { Enseignement } from '../../../models/enseignement.model';
import { State, Store } from '@ngrx/store'

//reducer
import * as trackPlayedReducer from './../../../reducers/track-played/track-played.reducer';
import * as trackPlayedAction from './../../../reducers/track-played/track-played.actions';

@Component({
  selector: 'audio-display',
  templateUrl: './audio-display.component.html',
  styleUrls: ['./audio-display.component.scss']
})
export class AudioDisplayComponent implements OnInit {

  public track: Enseignement = EnseignementConstant.DEFAULT;
  public player: HTMLAudioElement;
  public onPlayed: boolean;

  public duration: number = 0;    // Durée totale
  public time: number = 0;    // Temps écoulé
  public fraction: number = 0;
  public percent: number = 0;
  public isDisplay: boolean = false;

  public env: any;

  //slider prams
  public max = 100;
  public min = 0;
  public step = 1;
  public thumbLabel = false;
  public value = 0;

  constructor(private _store: Store<trackPlayedReducer.State>, public commonService: CommonService) {
    this.env = environment;
    this._store.select(trackPlayedReducer.getTrackPlayed).subscribe((data: TrackPlayed) => {
      if (data && data.player && data.track) {
        this.track = data.track;
        this.player = data.player;
        this.initPlayer();
      }
    });
  }

  ngOnInit() {
  }

  public initPlayer() {

    if (this.player) {

      this.player.addEventListener("play", () => {
        this.isDisplay = true;
        this.onPlayed = true;
      });

      this.player.addEventListener("pause", () => {
        this.onPlayed = false;
      });

      this.player.addEventListener("timeupdate", () => {
        this.update();
      });

    }
  }

  public update() {
    this.duration = this.player.duration;    // Durée totale
    this.time = this.player.currentTime; // Temps écoulé
    this.fraction = this.time / this.duration;
    this.percent = Math.ceil(this.fraction * 100);
    this.value = this.percent;
  }

  public onSliderChange(slider) {
    let duration = this.player.duration;
    this.player.currentTime = (duration * slider.value) / 100;
  }

}
