import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AudioDisplayComponent } from './audio-display.component';

describe('AudioDisplayComponent', () => {
  let component: AudioDisplayComponent;
  let fixture: ComponentFixture<AudioDisplayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AudioDisplayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AudioDisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
