import { Component, Input } from '@angular/core';
import { State, Store } from '@ngrx/store';
import { TrackPlayed } from './../../../../models/track-played.model';

//reducer
import * as trackPlayedReducer from './../../../../reducers/track-played/track-played.reducer';
import * as trackPlayedAction from './../../../../reducers/track-played/track-played.actions';

@Component({
  selector: 'widget-progressbar',
  templateUrl: './widget-progressbar.component.html',
  styleUrls: ['./widget-progressbar.component.scss']
})
export class WidgetProgressComponent {
  public player: HTMLAudioElement;

  public duration: number = 0; // Durée totale
  public time: number = 0; // Temps écoulé
  public fraction: number = 0;
  public percent: number = 0;

  public progressClass: string = 'progress-bar progress-bar-striped progress-bar-animated';

  constructor(private _store: Store<trackPlayedReducer.State>) {
    this._store
      .select(trackPlayedReducer.getTrackPlayed)
      .subscribe((data: TrackPlayed) => {
        this.player = data.player;
        this.initProgress();
      });
  }

  ngOnInit() {}

  public initProgress() {
    if (this.player) {
      this.player.onplay = () => {
        this.progressClass =
          'progress-bar progress-bar-striped progress-bar-animated';
      };

      this.player.onpause = () => {
        this.progressClass = 'progress-bar progress-bar-striped';
      };

      this.player.ontimeupdate = () => {
        this.update();
      };
    }
  }

  update() {
    this.duration = this.player.duration; // Durée totale
    this.time = this.player.currentTime; // Temps écoulé
    this.fraction = this.time / this.duration;
    this.percent = Math.ceil(this.fraction * 100);
  }
}
