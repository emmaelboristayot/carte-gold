import { TrackPlayed } from './../../../models/track-played.model';
import { CommonService } from './../../../shared/common.service';
import { EnseignementConstant } from './../../../constants/enseignement.constant';
import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
//model
import { Enseignement } from '../../../models/enseignement.model';
import { State, Store } from '@ngrx/store'

//reducer
import * as trackPlayedReducer from './../../../reducers/track-played/track-played.reducer';
import * as trackPlayedAction from './../../../reducers/track-played/track-played.actions';

@Component({
  selector: 'audio-widget',
  templateUrl: './audio-widget.component.html',
  styleUrls: ['./audio-widget.component.scss']
})
export class AudioWidgetComponent implements OnInit {
  public track: Enseignement = EnseignementConstant.DEFAULT;

  public player: HTMLAudioElement;
  public trackPlayed: TrackPlayed;
  public onPlayed: boolean;

  public duration: number = 0;    // Durée totale
  public time: number = 0;    // Temps écoulé
  public isDisplay: boolean = false;

  public modal: any;

  constructor(
    private _store: Store<trackPlayedReducer.State>,
    private commonService: CommonService,
    private router: Router
  ) {
    this._store.select(trackPlayedReducer.getTrackPlayed).subscribe((data: TrackPlayed) => {
      if (data && data.player && data.track) {
        this.trackPlayed = data;
        this.player = data.player;
        this.initPlayer();
      }
    });

  }

  ngOnInit() {
    this.router.events
    .subscribe((event) => {
        if (event instanceof NavigationEnd && event.url === '/') {
            this.isDisplay = false;
          }
    });
  }

  public initPlayer() {

    if (this.player) {

      this.player.addEventListener("play", () => {
        this.isDisplay = true;
        this.onPlayed = true;
      });

      this.player.addEventListener("pause", () => {
        this.onPlayed = false;
      });

      this.player.addEventListener("timeupdate", () => {
        this.update();
      });

    }
  }

  public update() {
    this.duration = this.player.duration;    // Durée totale
    this.time = this.player.currentTime; // Temps écoulé
  }

  public openAudioDisplay() {
    this.modal = $('#audioDisplayModal');
    this.modal.modal({
      keyboard: false,
      backdrop: true,
      show: true
    });
  }

}
