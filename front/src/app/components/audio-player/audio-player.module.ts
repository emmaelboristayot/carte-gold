import { NgModule }       from '@angular/core';
import { CommonModule }   from '@angular/common';
import { LazyLoadImageModule } from 'ng-lazyload-image';
import { AudioTrackComponent } from './audio-track/audio-track.component';
import { ProgressBarComponent } from './progress-bar/progress-bar.component';
import { PlayPauseComponent } from './play-pause/play-pause.component';
import { ButtonLikeComponent } from './button-like/button-like.component';
import { ButtonOptionComponent } from './button-option/button-option.component';
import { MatButtonModule, MatCardModule, MatMenuModule, MatIconModule, MatSliderModule } from '@angular/material';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { AudioWidgetComponent } from './audio-widget/audio-widget.component';
import { ImageCardComponent } from '../../shared/image-card/image-card.component';
import { WidgetProgressComponent } from './audio-widget/widget-progressbar/widget-progressbar.component';
import { AudioDisplayComponent } from './audio-display/audio-display.component';

@NgModule({
  imports: [
    CommonModule,
    MatButtonModule,
    MatCardModule,
    MatMenuModule,
    MatIconModule,
    MatSliderModule,
    AngularSvgIconModule,
    LazyLoadImageModule
  ],
  exports: [
    ProgressBarComponent,
    PlayPauseComponent,
    AudioTrackComponent,
    AudioWidgetComponent,
    ImageCardComponent,
    AudioDisplayComponent
  ],
  declarations: [
    AudioTrackComponent,
    ProgressBarComponent,
    PlayPauseComponent,
    ButtonLikeComponent,
    ButtonOptionComponent,
    AudioWidgetComponent,
    ImageCardComponent,
    WidgetProgressComponent,
    AudioDisplayComponent
  ],
  providers: [ ]
})
export class AudioPlayerModule {}