import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store'

//reducer
import * as authReducer from './../../../reducers/auth/auth.reducer';
import * as authAction from './../../../reducers/auth/auth.actions';

@Component({
  selector: 'form-login',
  templateUrl: './form-login.component.html',
  styleUrls: ['./form-login.component.scss']
})
export class FormLoginComponent implements OnInit {
  user: FormGroup;

  constructor(private fb: FormBuilder,
    private _store: Store<authReducer.State>
  ) {
    this._store.select(authReducer.getUser);
  }

  ngOnInit() {

    this.user = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]]

    });
  }

  onSubmit() {
    this._store.dispatch(new authAction.Login(this.user.value));
  }
}
