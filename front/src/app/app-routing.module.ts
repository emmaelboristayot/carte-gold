import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

// Components
import { TeachingPageComponent } from './components/teaching-page/teaching-page.component';
import { LoginPageComponent } from './components/login-page/login-page.component';
import { ThemeTeachingComponent } from './components/theme-teaching/theme-teaching.component';

// Services
import { AuthGuard } from './services/auth-guard.service';
import { UserService } from './services/user.service';

const appRoutes: Routes = [
  {
    path: 'enseignements',
    component: TeachingPageComponent,
    canLoad: [AuthGuard],
    canActivate: [AuthGuard]
  },
  {
    path: 'theme-enseignement/:id',
    component: ThemeTeachingComponent,
    canLoad: [AuthGuard],
    canActivate: [AuthGuard]
  },
  {
    path: '',
    component: LoginPageComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      appRoutes,
      { useHash: true }
    )
  ],
  exports: [
    RouterModule
  ],
  providers: [
    AuthGuard,
    UserService
  ]
})
export class AppRoutingModule { }