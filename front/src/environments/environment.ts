// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  api: 'http://localhost:8000/user',
  audio: './assets/',
  theme_image: './assets/',
  default_password: '1364cba01e0ee80ef4381175bd6cf0d3'
};
