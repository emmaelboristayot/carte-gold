-- phpMyAdmin SQL Dump
-- version 4.1.4
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Sam 17 Juin 2017 à 20:09
-- Version du serveur :  5.6.15-log
-- Version de PHP :  5.4.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `carte_gold`
--

-- --------------------------------------------------------

--
-- Structure de la table `abonnements`
--

CREATE TABLE IF NOT EXISTS `abonnements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `annee` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Contenu de la table `abonnements`
--

INSERT INTO `abonnements` (`id`, `annee`) VALUES
(1, '2015'),
(2, '2016'),
(3, '2017');

-- --------------------------------------------------------

--
-- Structure de la table `abonnes`
--

CREATE TABLE IF NOT EXISTS `abonnes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_inscrits` int(11) NOT NULL,
  `abonnements` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Contenu de la table `abonnes`
--

INSERT INTO `abonnes` (`id`, `id_inscrits`, `abonnements`, `date`) VALUES
(1, 1, '1,2,3', '2017-06-06'),
(2, 2, '1', '2017-06-05'),
(3, 3, '1,2,3', '2017-06-10');

-- --------------------------------------------------------

--
-- Structure de la table `auteurs`
--

CREATE TABLE IF NOT EXISTS `auteurs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `prenom` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `photo` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `fonction` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Contenu de la table `auteurs`
--

INSERT INTO `auteurs` (`id`, `nom`, `prenom`, `photo`, `fonction`, `description`) VALUES
(1, 'ANKOU', 'Lucas', '', '', '');

-- --------------------------------------------------------

--
-- Structure de la table `enseignements`
--

CREATE TABLE IF NOT EXISTS `enseignements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `soustitre` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `id_themes` int(11) NOT NULL,
  `resume` text COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `id_abonnements` int(11) NOT NULL,
  `id_auteurs` int(11) NOT NULL,
  `vues` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Contenu de la table `enseignements`
--

INSERT INTO `enseignements` (`id`, `titre`, `soustitre`, `id_themes`, `resume`, `image`, `date`, `id_abonnements`, `id_auteurs`, `vues`) VALUES
(1, 'La vie divine', '', 1, '', '', '2017-06-04', 1, 1, 9),
(2, 'heaven is now', '', 1, 'la paradie maintenant est possible.', '', '2017-06-15', 2, 1, 0),
(3, 'Construire sa vie en Christ', '', 3, 'Naitre et grandir dans le seigneur', '', '2017-06-08', 3, 1, 0),
(4, 'Le retour de Jesus', '', 4, 'Il revient pour emporter son Eglise', '', '2017-06-04', 2, 1, 0);

-- --------------------------------------------------------

--
-- Structure de la table `favoris`
--

CREATE TABLE IF NOT EXISTS `favoris` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_abonnes` int(11) NOT NULL,
  `enseignements` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Contenu de la table `favoris`
--

INSERT INTO `favoris` (`id`, `id_abonnes`, `enseignements`) VALUES
(1, 1, '2'),
(2, 2, '1,2'),
(3, 3, '1,2,3,4');

-- --------------------------------------------------------

--
-- Structure de la table `inscrits`
--

CREATE TABLE IF NOT EXISTS `inscrits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `prenom` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `telephone` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `statut` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Contenu de la table `inscrits`
--

INSERT INTO `inscrits` (`id`, `nom`, `prenom`, `telephone`, `email`, `statut`, `password`, `created_at`, `updated_at`) VALUES
(1, 'TAYOT', 'Emmael', '0677839365', 'emmaelboristayot@yahoo.fr', 'n/s', 'ee19723d3d4ca3d32891eb059757dbd4', '2017-06-06 14:31:15', '2017-06-09 15:23:34'),
(2, 'BILOGHE', 'Mariska', '0651888965', 'mariskabiloghe@yahoo.fr', 'n/s', 'Emmael10?', '2017-06-06 14:31:15', '0000-00-00 00:00:00'),
(3, 'KOUMBA', 'Maurice', '0456677889', 'k.maurice@gmal.com', 's', 'Koumbilove2000', '2017-06-06 14:31:15', '0000-00-00 00:00:00'),
(4, 'NGEMA', 'Hector', '0729722387', 'n.hector@gmal.com', 's', '7c3444ad66e80e080bad8c8994ec23b8', '2017-06-09 15:28:44', '2017-06-09 15:28:44');

-- --------------------------------------------------------

--
-- Structure de la table `notes`
--

CREATE TABLE IF NOT EXISTS `notes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_enseignements` int(11) NOT NULL,
  `id_abonnes` int(11) NOT NULL,
  `resume` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_enseignements` (`id_enseignements`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Contenu de la table `notes`
--

INSERT INTO `notes` (`id`, `id_enseignements`, `id_abonnes`, `resume`, `created_at`, `updated_at`) VALUES
(3, 2, 1, 'comment TERERE', '2017-06-09 21:44:34', '2017-06-09 21:44:34'),
(2, 1, 1, 'Christ est roi', '2017-06-09 21:50:04', '2017-06-09 21:50:04');

-- --------------------------------------------------------

--
-- Structure de la table `players`
--

CREATE TABLE IF NOT EXISTS `players` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_abonnes` int(11) NOT NULL,
  `ecoutes` text COLLATE utf8_unicode_ci NOT NULL,
  `en_cours` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_abonnes` (`id_abonnes`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Contenu de la table `players`
--

INSERT INTO `players` (`id`, `id_abonnes`, `ecoutes`, `en_cours`, `created_at`, `updated_at`) VALUES
(2, 1, ',2|2017-06-10 00:29:18,1|2017-06-10 00:34:13', '1,300', '2017-06-09 22:34:13', '2017-06-09 22:34:13');

-- --------------------------------------------------------

--
-- Structure de la table `suggestions`
--

CREATE TABLE IF NOT EXISTS `suggestions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_enseignements` int(11) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `themes`
--

CREATE TABLE IF NOT EXISTS `themes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Contenu de la table `themes`
--

INSERT INTO `themes` (`id`, `titre`, `description`, `image`) VALUES
(1, 'foi', '', ''),
(2, 'guerison', '', ''),
(3, 'Nouvelle naissance', '', ''),
(4, 'Le bapteme du St Esprit', '', '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
